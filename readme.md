
# Manual Leapfrog Creatr HS 
<br/>
<center><img src="./IMG/img1.png" /></center>



<br/>
<br/>
<br/>





Manual original Leapfrog 3D Printers - Manual Creatr HS - EN  
[Leapfrog 3D Printers - Manual Creatr HS - EN (PDF)](./Leapfrog%3D%Printers%-%Manual%Creatr%HS%-%EN.pdf)
<small><i>http://support.lpfrg.com/support/solutions/1000108213</small></i>



    
<br/>
<br/>   

******  

## Continguts
- [Manual Leapfrog Creatr HS](#manual-leapfrog-creatr-hs)
  * [Continguts](#continguts)
  * [1 Especificacions tècnicas](#1-especificacions-t-cnicas)
  * [1.2 Components](#12-components)
  * [1.3 Components electronics](#13-components-electronics)
    + [1.3.1 PCB Hotend](#131-pcb-hotend)
    + [1.3.2 Olimex](#132-olimex)
    + [1.3.3 Marlin](#133-marlin)
      - [Firmware Update (Motor Control Board)](#firmware-update--motor-control-board-)
  * [1.4 Caracteristicas tècnicas](#14-caracteristicas-t-cnicas)
  * [2. Software (Slicer)](#2-software--slicer-)
    + [2.3 CURA](#23-cura)
      - [2.3.1 Instal·lació](#231-instal-laci-)
      - [2.3.2 Connexió PC - Enviar comandes G-CODE per USB](#232-connexi--pc---enviar-comandes-g-code-per-usb)
      - [2.3.3 Control Manual al cura "Interface limitada"](#233-control-manual-al-cura--interface-limitada-)
      - [2.3.4 Paràmetres  (Recomanats per FabLab)](#234-par-metres---recomanats-per-fablab-)
    + [2.4 Simplify 3D](#24-simplify-3d)
      - [2.4.1 Instal·lació](#241-instal-laci-)
      - [2.4.2 Perfils](#242-perfils)
      - [2.4.3 Connexió PC - Enviar comandes G-CODE per USB](#243-connexi--pc---enviar-comandes-g-code-per-usb)
        * [2.4.4 Control manual al Symplify](#244-control-manual-al-symplify)
        * [2.4.5 Impressió](#245-impressi-)
    + [2.5 Pronterface](#25-pronterface)
      - [2.5.1 Instal·lació](#251-instal-laci-)
    + [3. G-Code](#3-g-code)
  * [3 Filaments](#3-filaments)
    + [3.1 Tipus de filament](#31-tipus-de-filament)
    + [3.2 Canvi filament "segons fabricant"](#32-canvi-filament--segons-fabricant-)
    + [3.3 Canvi filament "manual" (Recomanat)](#33-canvi-filament--manual---recomanat-)
    + [3.4 Problemes comuns al carregar filament](#34-problemes-comuns-al-carregar-filament)
  * [4 HotEnd](#4-hotend)
    + [4.1 Esquema](#41-esquema)
      - [Muntatge](#muntatge)
      - [Extracció de la resistència](#extracci--de-la-resist-ncia)
      - [Extracció del termistor](#extracci--del-termistor)
  * [5. Headbed](#5-headbed)
    + [5.1 Calibracio](#51-calibracio)
    + [5.2 Anivellació respecte al llit](#52-anivellaci--respecte-al-llit)
    + [5.2 Ajust sensor inductiu de l'eix Z](#52-ajust-sensor-inductiu-de-l-eix-z)
      - [Mètode 1](#m-tode-1)
      - [Mètode 2](#m-tode-2)
  * [5.3 Neteja](#53-neteja)
    + [5.3.1 Neteja el llit](#531-neteja-el-llit)
    + [5.3.2 Neteja (unclog) de l'extrusor](#532-neteja--unclog--de-l-extrusor)
      - [Mètode 1](#m-tode-1-1)
      - [Mètode 2](#m-tode-2-1)
    + [5.3.3 Neteja extrusor (motor)](#533-neteja-extrusor--motor-)
  * [5.4 Refrigeració](#54-refrigeraci-)
  * [6. Resolucio de problemes](#6-resolucio-de-problemes)
    + [6.1  Eines](#61--eines)
    + [6.2 Deixa de extruir](#62-deixa-de-extruir)
    + [6.3 Mala qualitat en les impressions](#63-mala-qualitat-en-les-impressions)
    + [6.4 La peça es desenganxa](#64-la-pe-a-es-desenganxa)
    + [6.5 Per què els meus motors pas a pas de l'eix Y no funcionen?](#65-per-qu--els-meus-motors-pas-a-pas-de-l-eix-y-no-funcionen-)
    + [6.6 Com comprovar la funcionalitat dels motors pas a pas X i Y](#66-com-comprovar-la-funcionalitat-dels-motors-pas-a-pas-x-i-y)
    + [6.7 Com comprovar la funció dels interruptors limitadors (End Stop)](#67-com-comprovar-la-funci--dels-interruptors-limitadors--end-stop-)
    + [6.8 Sota extrusió](#68-sota-extrusi-)
      - [Anell de pressió massa fluix](#anell-de-pressi--massa-fluix)
      - [L'anell de pressió és massa ajustat](#l-anell-de-pressi---s-massa-ajustat)
      - [Osques al filament](#osques-al-filament)
      - [L'engranatge de transmissió està desalineat](#l-engranatge-de-transmissi--est--desalineat)
      - [Eix del motor de l'extrusora danyat](#eix-del-motor-de-l-extrusora-danyat)
    + [6.9 Per què s'imprimeix a l'aire després de poques capes?](#69-per-qu--s-imprimeix-a-l-aire-despr-s-de-poques-capes-)
    + [6.10 Sobreextrusió](#610-sobreextrusi-)
      - [La temperatura és massa alta](#la-temperatura--s-massa-alta)
      - [Multiplicador d'extrusió (Simplify3D) / Factor d'extrusió massiva](#multiplicador-d-extrusi---simplify3d----factor-d-extrusi--massiva)
      - [Qualsevol de la configuració d'extrusió de subcategoria superior al 100%](#qualsevol-de-la-configuraci--d-extrusi--de-subcategoria-superior-al-100-)
      - [Configuració dels nozzles i configuració de l’amplada del fil dels nozzles](#configuraci--dels-nozzles-i-configuraci--de-l-amplada-del-fil-dels-nozzles)
      - [Nozzle danyat](#nozzle-danyat)
      - [Cal actualitzar el el firmware](#cal-actualitzar-el-el-firmware)
      - [Cal modificar el firmware](#cal-modificar-el-firmware)
      - [Dimensió incorrecta del filament](#dimensi--incorrecta-del-filament)
    + [Funcionament del sensor de temperatura d'una impressora 3D](#funcionament-del-sensor-de-temperatura-d-una-impressora-3d)
    + [6.11 Calibratge del llit impossible](#611-calibratge-del-llit-impossible)
    + [6.12 Un dels extrusors no escalfa](#612-un-dels-extrusors-no-escalfa)
  * [7. Substituir peces](#7-substituir-peces)
    + [7.1 Com canviar els ventiladors del capçal](#71-com-canviar-els-ventiladors-del-cap-al)
    + [7.2 Com substituir el llit](#72-com-substituir-el-llit)
    + [7.3 Com substituir els coixinets de la impressora](#73-com-substituir-els-coixinets-de-la-impressora)
    + [7.4 Substitució del conjunt electrònic de la impressora](#74-substituci--del-conjunt-electr-nic-de-la-impressora)
    + [7.5 Com substituir el subconjunt del cable de la cinta](#75-com-substituir-el-subconjunt-del-cable-de-la-cinta)
    + [7.6 Com substituir el PCB de la vostra impressora Creatr HS](#76-com-substituir-el-pcb-de-la-vostra-impressora-creatr-hs)
    + [7.7 Com substituir les unitats d'unitat de filament al Creatr HS](#77-com-substituir-les-unitats-d-unitat-de-filament-al-creatr-hs)
    + [7.8 Canvi del termistor i de la resistència](#78-canvi-del-termistor-i-de-la-resist-ncia)
    + [7.9 Com reemplaçar les Unitats d'unitats de filament a la Creatr HS](#79-com-reempla-ar-les-unitats-d-unitats-de-filament-a-la-creatr-hs)
    + [7.10 Com instal·lar els ventiladors de refrigeració de la unitat d’acció Filament](#710-com-instal-lar-els-ventiladors-de-refrigeraci--de-la-unitat-d-acci--filament)
    + [7.11 Com substituir el sensor d’inducció Z-Stop](#711-com-substituir-el-sensor-d-inducci--z-stop)
    + [7.12 Substituir Rotary encoder](#712-substituir-rotary-encoder)
- [3. Millores y curiositats](#3-millores-y-curiositats)




******  

## 1 Especificacions tècnicas

## 1.2 Components


![image](./IMG/img2.png)

**1.** Rotary encoder  
**2.** Bobina Filament 1.75 mm x 2   
**3.** Heatbed  
**4.** 2 x Eix X  
**5.** Extrusor Esquerra (Left)  
**6.** Guias filament (Tubs PFTE; 2mm ID,  3mm OD)  
**7.** Extrusor Dreta (Right)  
**8.** 2 x Eix Y  
**9.** Conector pnemuatic PC4-M6  
**10.** Nozzle 0.4 mm   
**11.** Port USB  
**12.** Pantalla     


    
<br/>
<br/>   

| **Component**    | **ID** | **Descripció**  |   |
|:---|:---:|---|:---:|
| Electronics board - LMC Creatr HS  | **B-01-1207**  | Motorcontrolboard (LMC)  |<img src="./IMG/B-01-1207.png" width="250"/> |
| NEMA 17 stepper motor 48mm  |  **C-01-1046** | Extruder motor (2), X-motor (1), Y-motor (2)  | <img src="./IMG/C-01-1046.png" width="250"> |
|  Thermistor Assembly Creatr HS | **SP-01-1019**  | Temperature Sensor hot end Creatr HS and Xeed 100K NTC Thermistor | <img src="./IMG/SP-01-1019.png" width="250">  |
| Heater cartridge  |**SP-01-1020**   | 40W 24V heater for Creatr Hot end  |  <img src="./IMG/SP-01-1020.PNG" width="250"> |
| Nozzle Block 0.4mm (set of 2) | **SP-01-1007**  | Hot end including PTFE and Brass setscrew. Without electronics. Nozzle size 0.35mm(Mod:0.4)  | <img src="./IMG/SP-01-1007.png" width="250">  |
| Nozzle Block 0.5mm (set of 2)|  | **SP-01-1008**  | Hot end including PTFE and Brass setscrew. Without electronics. Nozzle size 0.5mm  | <img src="./IMG/C-01-1046.png" width="250">  |
| Cable to printhead\ |  **C-01-1150** |   |  <img src="./IMG/C-01-1150.png" width="250"> |
| 4 wire/cable set of 5  | **3 pcs: C-01-1241** ;  **1 pcs: C-01-1242** ;  **1 pcs: C-01-1243**  | 3 pcs (1400mm) X motor, Right Y motor, Right E motor;   1 pcs (1000mm) Left Y motor;   1 pcs (1100mm) Left E motor  | <img src="./IMG/C-01-1046.png" width="250">  |
| USB double FTDI cable|  **USB NMC-2.5M** |   |   |  <img src="./IMG/C-01-1046.png" width="250"> |
| Y carriage plastic  | **C-01-1131**  |  Y-axis bearing block | <img src="./IMG/C-01-1131.png" width="250">  |
| HS heat sink Print head   |  **C-01-1135** | Aluminium Printhead heat sink. Mounting for two hot ends possible  |  <img src="./IMG/C-01-1135.png" width="250"> |
| Printhead PCB   |  **B-01-1152** |  Head PCB that fits inside the Creatr HS (XL) printhead |<img src="./IMG/B-01-1152.png" width="250">|
| Extruder plastic part  |  **C-01-1134** |  Plastic bearing frame for printhead  | <img src="./IMG/C-01-1134.png" width="250">  |
|   |   |   |   |


<br/>

![image](./IMG/img3.png)
![image](./IMG/img4.png)

<br/>

## 1.3 Components electronics
### 1.3.1 PCB Hotend
![PCB Hotend](./IMG/B-01-1152.png)  
![PCB Hotend](./IMG/pcbhotend.png)  

<br/>

### 1.3.2 Olimex
![Olimex_a](./IMG/img19.png)   
![Olimex_b](./IMG/img20.png)   
![Olimex_c](./IMG/img21.png)   
![Olimex_d](./IMG/img22.png)  

### 1.3.3 Marlin
![Marlin](./IMG/marlin.png)  

#### Firmware Update (Motor Control Board)  

**Eines**  
- Dispositiu de memòria USB.  
- Asseguri's que la seva impressora estigui executant la versió 1.4.2 o superior . VOSTÈ HA DE TENIR AQUEST SOFTWARE INSTAL·LAT PRIMER. Per a verificar-ho seleccioni el botó d'informació en la seva pantalla:  <br>
![info](./IMG/info.png)  

La informació proporcionada aquí li dirà quina versió de programari té:  
![info](./IMG/infomachine.jpg)  
**(Foto antiga: mostra que la versió de software és 1.4.2 i la versió de firmware és 2.2).**  
El software és universal per a tots els models de la sèrie Creatr HS. No obstant això, cada model d'impressora té un software específic; asseguri's de descarregar el microprogramari correcte relacionat amb el model d'impressora que posseeix.  
Per a actualitzar el firmware, faci el següent:  
1. Descarregui l'arxiu .HEX apropiat per al seu model i guardi-ho en una memòria USB.
2. Encengui la impressora i esperi que la pantalla arribi al menú principal.
3. Col·loqui l'USB en el panell frontal de la impressora i seleccioni la memòria USB.
4. Despres, seleccioni "CreatrHS_Firmware_2.5.hex" i carregui-ho en la impressora.
5. La pantalla mostrarà "Actualitzant PCB ...". Quan acabi, mostrarà "Actualització reeixida; reiniciant".  <br>
**Pressioni el botó per a tornar al menú principal.**  
6. Reiniciï la màquina i verifiqui el menú Informació per a assegurar-se que s'hagi actualitzat el microprogramari.
  
   Ref: http://cloud.lpfrg.com/support/creatrhs/Software/Firmware/CreatrHS_Firmware_2.5.zip



## 1.4 Caracteristicas tècnicas


|**Caracteristica**                  | **Valor**   |
|---|---:|
| **Physical dimensions (DWH )**    | 633 x 486 x 523 mm   |
| Peso                            | 32 kg     |
| Input voltage range             | 115-250 Volts   |
| Input frequency                 | 47 - 63 Hz   |
| Power consumption                 | 395W   |
| Build size single extruder (DWH)  | 280 x 270 x 180 mm   |
| Build size dual extruder (DWH)  | 280 x 240 x 180 mm   |
| Build volume                      | 13.6 L      |
| Heated bed max temp             | 90 °C     |
| Heated Chamber                  | No   |
| Hot end Max temp                  | 275 °C   |
| Number of extruders               | 2  |
| Extruder size(s)                  | 0.4 mm    |
| Filament size                     | 1.75 mm     |
| Grosor capa                     | [0.1 - 0.3] mm   |
| Max print speed (DW)              | 18000 mm / min (100 mm / s)   |
| Max. travel speed (DW)          | 24000 mm / min (400 mm / s)  
| Stepper motors                  | 1.8° Step angle with 1/32 micro stepping   |
| Positioning accuracy (DW)         | 0.017 mm|
| Positioning accuracy (H)          | 0.020 mm   |
| Body/frame construction           | Aluminum Extrusion Profiles   |
| Heated bed construction         | High grade glass   |
| Printbed Z-levelling            | 4-knob hand levelling  |
| Internal OS                     | Linux  |
| USB standalone format             | Gcode |
| USB connection to computer      | Virtual COM port |
| Printable materials               | PLA, ABS, PET-G, |

<br/>
DWH: Profunditat, amplada, alçada. Referència del sistema de coordenades X, Y, Z 
<br/>
<br/>


## 2. Software (Slicer)

Precisament aquí és on entra en paper el Slicer (o "rebanador"). El Slicer calcula les interseccions de l'objecte 3D dels plans a l'altura de capa d'impressió, i determina les seccions que obté. Ho divideix en "llesques", d'aquí el seu nom.  

Slicers mes comuns per Leapfrog Creatr HS

|                 | Cura | Simplify3D  |Pronterface |
|---|---|---|---|
| Cost            |  Gratuit |  100USD | Gratuit  |
| Control manual  | Si       | Si      | Si  |
| Perfil          |  Si      | Si      | No  |


### 2.3 CURA  
#### 2.3.1 Instal·lació
1. Descarregar Cura.  
 Ref: https://ultimaker.com/es/software/ultimaker-cura  <br><br>
<img src= "./IMG/CapturaCura1.png" width="700"><br><br>
2. Afegir una impressora que no sigui de xarxa  <br>
<img src= "./IMG/CapturaCura2.png" width="600"><br><br>
3. Afegiu una impressora i trieu "Leapfrog B.V." i després "Leapfrog Creatr HS".  <br> 
<img src= "./IMG/CapturaCura3.png" width="600"> <br><br>
4. Un cop afegida la impressora entrem en "Machine Settings"<br>
<img src = "./IMG/MachineSettingscura.PNG"><br><br>
5. Modificar "Nozzle size" de "0.35" a "0.4"<br>
<img src = "./IMG/MachineSettingscura2.png"><br><br><br>

**Interfície**  <br><br>
<img src= "./IMG/fotocura01.png" width="900">  <br><br>
Podem deshabilitar l'extrusor 2 si no es si no s'utilitzarà, per evitar confusions.
<img src= "./IMG/fotocura02.png" width="900">  
<br>

#### 2.3.2 Connexió PC - Enviar comandes G-CODE per USB  

1. Seleccionar el menú de "standalone" a la impressora i connectar-la mitjançant el port USB B que es troba a la part posterior de la màquina, i connectar a l'ordinador.
2. Obrir "Ultimaker Cura" i seleccionar el perfil d'impressora a la qual anemi a connectar.
3. Seleccionar la pestanya "Preview".
<br>  

#### 2.3.3 Control Manual al cura "Interface limitada"   
1.  Seleccionar el menú de "standalone" a la impressora i connectar-la mitjançant el port USB B que es troba a la part posterior de la màquina, i connectar a l'ordinador.  
<img src= "./IMG/Imagen29.png" width="300">  <br>
2. Obrir Cura i seleccionar mode monitor.  
<img src= "./IMG/curamonitor01.png" width="650">  <br>


#### 2.3.4 Paràmetres  (Recomanats per FabLab)
<pre>
<b>Printer</b>
X (Width): 230mm  
Y (Depth): 260mm  
Z (Height): 200mm  
Build plate shape: Rectangular  
Origin at center: Disabled  
Heated bed: Enabled  
Heated build volume: Disabled  
G-code flavor: Marlin  

<b>Printhead Settings</b>
X min: -66mm  
Y min: -52mm  
X max: 66mm  
Y max: 90mm  
Gantry Height: 2mm  
Number of Extruders: 1 or 2 (dual)  
Shared Heater: Disabled  

<b>Start G-code</b>  
G28 ;Home  
G1 Z15.0 F6000 ;Move the platform down 15mm  
;Prime the extruder  
G92 E0  
G1 F200 E3  
G92 E0  
  
<b>End G-code</b>
M104 S0  
M140 S0  
;Retract the filament  
G92 E1  
G1 E-1 F300  
G28 X0 Y0  
M84  

<b>Extruder 1</b>
Nozzle size: 0.4mm (standard)  
Compatible material diameter: 1.75mm  
Nozzle offset X: -33mm  
Nozzle offset Y: 0mm  
Cooling Fan Number: 0  

<b>Extruder 2</b>  
Nozzle size: 0.4mm  (standard)  
Compatible material diameter: 1.75mm  
Nozzle offset X: 0mm  
Nozzle offset Y: 0mm  
Cooling Fan Number: 1  
</pre>  

### 2.4 Simplify 3D  
#### 2.4.1 Instal·lació
1. Descarregar Symplify.
Ref: https://www.simplify3d.com/buy-now/  

#### 2.4.2 Perfils
PLA (Left/Right)
   - 0.25 mm
   - 0.20 mm
   - 0.10 mm
  
[Perfil PLA](http://support.lpfrg.com/helpdesk/attachments/1034586055)  

ABS (Left/Right)
   - 0.25 mm
   - 0.20 mm
   - 0.10 mm

[Perfil ABS](https://lfa.freshdesk.com/helpdesk/attachments/11001597577)

La impressora Creatr està disponible en un model d’extrusora única i en un model d’extrusora dual.

![image](./IMG/simplify0.png)  

#### 2.4.3 Connexió PC - Enviar comandes G-CODE per USB  
1. Seleccionar el menú de "standalone" a la impressora i connectar-la mitjançant el port USB B que es troba a la part posterior de la màquina, i connectar a l'ordinador
2. Obrir "Simplify3D" i buscar el menú d'eines i seleccionar "Machine control panell" (Ctrl + P)
3. Haureu de fer clic al botó "Actualitzar" perquè aparegui el nou port. Al Windows, normalment s’anomena COM #. Els usuaris de Mac veuran diversos ports, així que assegureu-vos de seleccionar el que porti USB al seu nom. No haureu de canviar la velocitat de transmissió, ja que la configuració predeterminada de 115200 és correcta. Un cop seleccionat el port correcte, feu clic a "Connect". Si tot va bé, el botó de connexió vermell hauria de posar-se verd, cosa que demostra que ara esteu connectat a la impressora.
<br><br>

![image](./IMG/simplify1.png)
  
##### 2.4.4 Control manual al Symplify

Podeu utilitzar el tauler de control de la màquina per controlar manualment la màquina en preparació per a la impressió.  
El primer que podeu comprovar és que els motors estan connectats i funcionen correctament.  
Per fer-ho, feu clic a la pestanya "Jog Controls", que mostrarà el tauler de control de jog, on podreu moure el capçal d'impressió manualment.  
Podeu moure-us en increments de 0,1, 1, 10 o 100 mm al llarg de qualsevol dels eixos, així com retreure o extruir el filament.  
Recordeu que sempre moveu el capçal d’impressió en relació amb tota la resta. Per exemple, si feu clic en un dels botons + Z, el capçal d’impressió s’allunyarà de la placa de construcció. Per al Creatr, això significa que la placa de construcció es desplaçarà cap avall.
  
![image](./IMG/simplify2.png)  

Tant els extrusors com el llit escalfat de la impressora també es poden controlar mitjançant.  
Els llums vermells al costat de l’extrusora i la temperatura del llit escalfat indiquen quan s’encén qualsevol d’aquests escalfadors. Podeu seleccionar l'extrusor dret o esquerre mitjançant l'opció "Cap d'eines actiu".  
Hi hauria d’haver un plàstic PLA muntat a l’extrusora adequada. A efectes de proves, seleccioneu l'extrusora adequada i introduïu "210" al camp de temperatura de l'extrusora i "50" al camp del llit escalfat. Quan premeu "Activat" per l'extrusora o el llit escalfat, el cercle vermell s'hauria d'il·luminar i la temperatura hauria de començar a augmentar. Tingueu en compte que el llit escalfat trigarà més a escalfar-se que el noozle.

![image](./IMG/simplify3.png)  

Podeu utilitzar la pestanya "Gràfic de temperatura" per veure una visualització en directe de les temperatures actuals de la impressora.  
La línia vermella indica la temperatura de l’extrusora i la línia blava indica la temperatura de la placa de construcció. Les línies de punts indiquen les temperatures de consigna desitjades.  
Abans de provar l'extrusió del nozzle, assegureu-vos que la temperatura de l'extrusora estigui escalfada i estabilitzada.  
Torneu a la pestanya Jog Controls i feu clic al botó d'extrusió de 100 mm. Mireu el cap com s’extreu el filament.  
Normalment és una bona idea preescalfar i preparar el nozzle seguint aquests passos abans de començar la impressió.
  
![image](./IMG/simplify4.png)  

##### 2.4.5 Impressió
Ja pudeu importar un fitxer STL. Descarregueu el fitxer que vulgueu i importeu-lo al programa mitjançant el botó "Importa".  
El següent pas és centrar el model fent clic al botó "Organitzar".  
Ara podeu començar a configurar els paràmetres de segmentació afegint un procés. Si en algun moment necessiteu ajuda, el programari inclou una guia d’inici ràpid que descriu els procediments bàsics. Per trobar-lo, aneu a Ajuda> Guia d’inici ràpid des del programa.  
Configurar un procés FFF nou (FFF significa Fusion Filament Fabrication i aquest és el nom de la indústria per al tipus d’impressió 3D que utilitza aquesta impressora). Aquests paràmetres FFF determinen exactament com imprimeix el model i juga un paper fonamental en la qualitat de la impressió. Per crear un procés nou, heu de fer clic al botó "Afegeix". Apareixerà un procés com a "Process1" i apareixerà la finestra de configuració de FFF a la pantalla. Per a aquesta part senzilla, només necessitareu un procés FFF.  

El programari Simplify3D ha fet que la configuració per als usuaris de la impressora, mitjançant l’ús dels perfils preconfigurats. Seleccioneu el perfil ‘Leapfrog Creatr (extrusor dret)’ al menú desplegable "Selecciona perfil" i "PLA" per al contenidor "Configuració automàtica per a material".  

Quan hàgiu acabat de canviar la configuració de FFF, podeu desar-les fent clic al botó "Desa". Després de desar, premeu el botó "Prepara" per crear les instruccions de la màquina necessàries per imprimir. El programari tallarà la peça en un fitxer de codi G que la impressora pot llegir. A continuació, el programari us demanarà que vegeu una vista prèvia de la impressió mitjançant el previsualitzador animat. Això us permetrà veure com seria la impressió abans d’imprimir-la realment.  

![image](./IMG/simplify5.png)  

Feu clic al signe vermell d'aturada per sortir de la vista prèvia. Apareixerà una finestra i us preguntarà si esteu preparat per imprimir. Seleccioneu Sí i apareixerà el MCP. La impressora pot fer una pausa durant uns segons per estabilitzar les temperatures abans d’imprimir. Un cop assolits els punts de consigna desitjats, començarà la impressió. En un tres i no res tindràs la primera part impresa.  

### 2.5 Pronterface 
#### 2.5.1 Instal·lació
1. Descarreguem de la pàgina oficial el programari i el descomprimim en l'escriptori.  
2. Ho executem, i seleccionem el port on aquest la nostra impressora i fem clic en "Connect" <br>
![image](./IMG/interfacepronterface.png)  
Ref: https://www.pronterface.com/

<br/>

### 3. G-Code

Ref: https://reprap.org/wiki/G-code  

L’objectiu principal és la fabricació additiva mitjançant processos FFF. Els codis per als moviments del capçal d’impressió segueixen l’estàndard del codi G del NIST RS274NGC, de manera que els firmwares RepRap tambe poden ser emparats per al fresat CNC i aplicacions similars.  

Hi ha algunes maneres diferents de preparar el codi G per a una impressora. Un mètode seria utilitzar un programa de tallat com Slic3r, Simplify3D o Cura.  Aquests programes importen un model CAD, stl, etc... el divideixen en capes (llesques, slices) i emeten el codi G necessari per imprimir cada capa.  
És possible editar i afegir comandes G-CODE al fitxer generat pel programa de slicing.  


**GCode habituals amb exemples**  
G28 – Perform Homing Routine  
>    G28 ; home all axes (X, Y, and Z)  
>    G28 X Y ; home X and Y axes  
>    G28 Z ; home Z axis only  

G90 and G91 – Set Positioning Mode
>   G90 ; use absolute positioning for the XYZ axes  
>    G1 X10 F3600 ; move to the X=10mm position on the bed  
>    G1 X20 F3600 ; move to X=20mm  

G91 ; use relative positioning for the XYZ axes  
>    G1 X10 F3600 ; move 10mm to the right of the current location  
>   G1 X10 F3600 ; move another 10mm to the right  

G1 – Linear Movement  
>    G1 X0 Y0 F2400 ; move to the X=0 Y=0 position on the bed at a speed of 2400 mm/min  
>    G1 Z10 F1200 ; move the Z-axis to Z=10mm at a slower speed of 1200 mm/min  
>    G1 X30 E10 F1800 ; push 10mm of filament into the nozzle while moving to the X=30 position at the same time  

G92 – Set Current Position  
>    G92 E0 ; set the current filament position to E=0  
>    G1 E10 F800 ; extrude 10mm of filament  

M104 and M109 – Extruder Heating Commands  
>    M104 S190 T0 ; start heating T0 to 190 degrees Celsius  
>    G28 X0 ; home the X axis while the extruder is still heating  
>    M109 S190 T0 ; wait for T0 to reach 190 degrees before continuing with any other commands  

M140 and M190 – Bed Heating Commands  
>    M140 S50 ; start heating the bed to 50 degrees Celsius  
>    G28 ; home all 3 axes while the bed is still heating  
>    M190 S50 ; wait until the bed reaches 50 degrees before continuing  

M106 – Set Fan Speed  
>    M106 S255 ; set the fan to full speed  
>    M106 S127 ; set the fan to roughly 50% power  
>    M106 S0 ; turn off the fan completely  

## 3 Filaments  
**La impressora Leapfrog per la configuració que té, utilitza filament de "1,75mm"**  
### 3.1 Tipus de filament  
|                 | Temperatura | Temperatura Bed  |Ventilació |
|---|---|---|---|
| PLA            |  190-220°C |  40°C - 60°C | 100%  |
| ABS  | 210 - 250°C       | 80 - 100°C      |  50% |
| PETG          |  220 - 250°C      | 50 - 80°C      | 100%  |

### 3.2 Canvi filament "segons fabricant"
1. Preescalfar el "hotend" amb el menú de "Preheat" de la impressora fins als 220 °C.  <br>
<img src= "./IMG/Capture41.jpg" width="100"><br>
2. Quan s'arribi a la temperatura, anar al menú de "Carrega y descarrega".  
<img src= "./IMG/menucambiofilamento01.png" width="200"><br>
3. Premer el boto de "Unload" per descarregar el filament al canal corresponent, repetir aquest proces fins que es pugui retirar.  
<img src= "./IMG/menucambiofilamento02.png" width="200"><br>
4. Retirar el filament y introduir el nou.  
5. Premer el boto de "load" per carregar el filament al canal corresponent, repetir aquest proces fins que comenci a sortir pero el "hotend".  
<br> <img src= "./IMG/menucambiofilamento03.png" width="200"> <br>

### 3.3 Canvi filament "manual" (Recomanat)
1. Preescalfar el "hotend" amb el menú de "Preheat" de la impressora fins als 220 ° C.  
<br> <img src= "./IMG/Capture41.jpg" width="100">
2. Quan s'arribi a la temperatura, estirar la palanca negra (imatge inferior) situat a la part posterior, el qual atrapa el filament.  
<br><img src= "./IMG/Capture42.jpg" width="200"><br>
3. Empènyer filament cap endavant 1 o 2 centímetres cap endavant fins que surti pel "Nozzle".  
4. Amb una tirada recta i ferm, llençar fins a descarregar el filament per complet.  
5. Deixeu anar la palanca quan hagi retirat tot el filament del tub guia.  
6. Talla la punta del filament que es va a carregar, el més recte possible, per facilitar la seva entrada.
7. Proveu que la punta del filament tan recta com sigui possible i insereixi pel tub guia.  
<br><img src= "./IMG/Capture43.jpg" width="400" /> <br>
8. Quan no deixi avançar mes, estirar la palanca (negra) de la part posterior per deixar passar el filament.  
9. Empenta el material fins que surti pel "nozzle" el nou filament.  

### 3.4 Problemes comuns al carregar filament
**Eines**:  
- Clau anglesa  12
- Alicates 
  - Embús de filament a "presilla" a l'entrada del motor extrusor.   <br>
  <img src= "./IMG/problemasfilamento03.jpg" width="300" /> <img src= "./IMG/problemasfilamento04.jpg" width="300" />  <br>  
  - Embús de filament a "presilla" a la sortida del motor extrusor.  
  <br><img src= "./IMG/problemasfilamento01.png" width="300" /> <img src= "./IMG/problemasfilamento02.png" width="300" />   <br>  
  - Embús de filament a entrada al "hot end".  
  <img src= "./IMG/problemasfilamento05.jpg" width="300" /> <br>  
 
******  
<br/>

## 4 HotEnd
### 4.1 Esquema
<br><img src= "./IMG/partshotend01.png" width="300"> <img src= "./IMG/partshotend02.png" width="300"><br>
<br><img src= "./IMG/partshotend03.png" width="300"><br>

  1. Barrel (*garganta*)
  2. Cartutx escalfador (heater)
  3. Sensor Temperatura
  4. Bloc
  5. Nozzle (boquilla)
  6. Tub PFTE (ID 1.0mm ; OD 2.00mm)
  7. Cargol

**NOTA**: El tub PFTE interior pot ser una causa per la qual les impressions no resultin òptimes.  Cal canviar-lo de forma periòdica.  Un problema d'extrusió pot ser d'aquí.  <br>
<br><img src= "./IMG/partshotend04.jpg" width="250"><br>
PFTE dreta en bon estat / PFTE esquerra nou.  

#### Muntatge

**Eines**
- Clau allen 1,5  
- Clau allen 2  
- Alicates  

<br>

1. Cargolar el "barrel" al bloc.   
2. Introduir el "nozzle" al "barrel" fins al final i comprovar que quedi ben enganxada a el bloc, però fem mitja volta al "barrel" per cargolar en calent.  
3. Inserir el cartutx escalfador en el bloc i prémer el cargol situat al inferior de l'bloc amb clau allen de 1,5.  
4. Cargolar el sensor de temperatura a la banda de l'cartutx escalfador.  
<br/>

Preescalfeu l'extrusor completament a 220º. Això ajuda a qualsevol filament a evitar que elimineu el nozzle; també crea prou calor per ampliar el dissipador de calor del capçal d’impressió per ajudar a treure l’extrusora en els passos posteriors.


  A continuació, heu de treure el nozzle de l'extrusor que voleu substituir. Per fer-ho, utilitzeu les tenalles per mantenir l'extrusora al seu lloc i utilitzeu la clau número 8 per treure el nozzle.

![Figura 28](./IMG/img28.PNG)  

Traieu l'extrusora

  Atenció! Les extrusores estaran molt calentes; si us plau, preneu les precaucions necessàries per evitar lesions.


  El següent pas és treure l'extrusora, per fer-ho, afluixant el cargol amb la clau Allen número 2 de la part superior dreta de l'extrusora, tal com es mostra a la figura 2 i, a continuació, traieu l'extrusora.

![Figura 13](./IMG/img13.jpg)  

#### Extracció de la resistència

  Un cop l'extrusora està fora, el següent pas és treure la resistència. Per fer-ho, afluixeu el cargol amb la clau Allen d'1,5.

![Figura 14](./IMG/img14.PNG)  

#### Extracció del termistor

  Per treure el termistor, haureu d'utilitzar una clau número 7 i desenganxar-lo per darrere de l'extrusora, tal com es pot veure a la imatge superior.


  En aquest moment, teniu dues opcions: podeu utilitzar el termistor i la resistència existents per a l’extrusora de reemplaçament o bé substituir-los també. Si decidiu conservar la resistència i el termistor, simplement munteu l’extrusor al capçal d’impressió de la mateixa manera l’ha eliminat.


Presteu també atenció al nivell dels extrusors

  Després de muntar l'extrusora / extrusores segons la caixa al capçal d'impressió, seguiu aquest enllaç i veure com alinear els extrusors a la mateixa alçada.

    
## 5. Headbed
### 5.1 Calibracio
**Eines**<br>
  Per calibrar el llit heatbed necessitareu:  
  • Paper  
  • Clau numero 8  
  • Clau numero 10  
  • Alcohol isopropílic  
  • Paper de cuina  
  • Rascleta  
    
1. Pre-escalfar la placa i el "nozzle".  
2. Ingressar en el menú de calibratge de llit.  
![bed_icon](./IMG/img5.png)  
3. Identificar les rodes de calibratge situades sota de la placa a cada cantonada.  
4. Polsar en continuar perquè es posicioni en el primer punt de referència.  
5. Ajustar amb l'ajuda d'un foli fins que entre el "nozzle" i el llit hi hagi una distància de "0.1 mm" el gruix d'un foli.   <br>
![Figura 6](./IMG/img6.jpg)  
![Figura 7](./IMG/img7.png) 
6. Polsar el "Rotaring encoder" per posicionar el següent punt de referència.  
7. Repetir el procés en cada cantonada fins que tingui la mateixa distància en tots els cantons.  
Aquest procés s’ha d’executar a cadascuna de les cantonades del llit.

 ![Figura 8](./IMG/img8.jpg)  

 ![Figura 9](./IMG/img9.jpg)

 ![Figura 10](./IMG/img10.jpg)  
Es recomana repetir aquests passos dues vegades per certificar un bon calibratge.  


### 5.2 Anivellació respecte al llit  
**Eines**
- Clau allen 2  
- Alicates 
<br>  

1. Identificar els cargols que subjecten el "barrel".  
<br><img src= "./IMG/tornillobarrel.jpg" width="200"><br>  
2. Amb l'ajuda d'una allen afluixar els cargols.  
3. Pujar el llit fins que els "Nozzles" quedin el més alineats possible.  
<br><img src= "./IMG/img12.PNG" width="200"><br>  
5.Ajustar fins que passi un foli per sota dels "Nozzles".  
<br><img src= "./IMG/Imagen24.jpg" width="200"><br>  
6. Fer "Preheat" i comprovar que passi un foli per sota dels "Nozzles".  
7. Amb alicates terminar d'ajustar.  
8. Apretar els cargols que subjecten el "barrel".  
9. Realitzar un calibratge de llit per comprovar que ha quedat correctament alineat. 


Us desitgem una bona impressió!
<br/>
### 5.2 Ajust sensor inductiu de l'eix Z

#### Mètode 1

1. Pujar manualment el heatbed, tirant de la corretja llit fins que el testimoni de l'eix Z és prengui.  
<img src= "./IMG/sensorz01.jpg" width="200" />
<img src= "./IMG/sensorz02.jpg" width="244" /> <br>
2. Comprovar el llit i el "nozzle" no es xoquin.  <br>
  2.2 Si es toquen?  <br>
  2.3 Amb el cargol de regulació del Z ajustar manualment fins que deixin xocar. <br> 
  2.4 No es toquen?  <br>
  2.5 Comprovar que la distància sigui inferior a un mil·límetre.  <br>
3. Un cop ajustat l'eix Z, fer "home" i anivellar el llit.

#### Mètode 2
** !! AVÍS! NO SEGUIR CORRECTAMENT AQUESTES INSTRUCCIONS POT DANYAR SERIEMENT LA IMPRESSORA; LA COMPENSACIÓ Z INCORRECTA PODRIA TENIR LA PREMSA DEL LLIT D'IMPRESSIÓ CALENTADA AL CAP D'IMPRESSIÓ QUE DANYA EL VIDRE, ELS BUCELLS, EL CAP D'IMPRESSIÓ I POSSIBLEMENT ALTRES PARTS. PER TANT, NO RECOMANEM FER AIX UN MENYS QUE LLEGIU A TRAVÉS I SEGUEIX CADA BIT DE LES INSTRUCCIONS PROPORCIONADES _ !! **  


La fàbrica que munta les impressores calibra el desplaçament en Z i el magatzem realitza diversos controls de qualitat aleatoris per verificar que les impressores estan realment calibrades. L'única raó per la qual hauríeu de tenir per ajustar el desplaçament Z seria si substituïu recentment el sensor d'inducció de l'eix Z ajustat accidentalment. Aquest enllaç de solució només s'hauria de dur a terme si trobeu que no podeu arribar al desplaçament Z anivellant el llit d'impressió.  

Com sabeu quan s’arriba a la compensació Z?  Si localitzeu el sensor d’inducció a la cantonada frontal esquerra de la impressora, notareu un petit forat al costat de la mateixa (és possible que l’haureu de mirar des d’un angle diferent ja que potser no està orientat cap a la part frontal). Quan es col·loca l’eix Z, el sensor d’inducció aviat entrarà en contacte elèctric amb el cargol metàl·lic situat a sota i una llum LED s’il·luminarà al forat del sensor d’inducció. Feu servir aquest LED per indicar-vos quan la impressora llegeix que l’eix Z es troba al mínim.  
![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1033522818/original/HS%20-%20Z%20induction%20sensor%20test.PNG?1446559320)  
1. Abans d'intentar-ho, primer heu d'intentar anivellar el llit. Això pot resoldre el problema sense haver de calibrar el desplaçament Z.
2. Engegueu la impressora i preescalfeu els nozzles. Quan els nozzles assoleixin com a mínim 210 graus, desconnecteu els tubs de filament amb filament del capçal d’impressió.

![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11001596197/original/HS%20-%20How%20to%20disconnect%20filament%20tube%20from%20print%20head.PNG?1455530792)  

3. Afluixeu els dos tubs de l'extrusora del capçal d'impressió amb una clau Allen de mida 2. [ATENCIÓ: les extrusores estan calentes i poden provocar cremades!]

![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11001595381/original/HS%20-%20hot%20end%20grub%20screw.PNG?1455529786)  

4. Amb la mateixa eina, premeu suaument el bloc de tub de l'extrusora per a cada extrusora fins que siguin uns 2-5 mm més baixos; si apliqueu massa pressió i els tubs no es mouen, podríeu acabar doblegant-los.
5. Quan els tubs d'extrusor estiguin a l'alçada correcta, estrenyiu els cargols amb la clau Allan de la mida 2; de nou, és molt important que no apliqueu cap pressió cap avall sobre cap de les parts esmentades.
6. El vostre desplaçament Z ja està configurat; caldrà fer ajustaments menors addicionals als quatre comandaments de vidre del llit d’impressió de cantonada. Això es pot fer mentre s'executa una impressió de prova.
<br/>

## 5.3 Neteja  
### 5.3.1 Neteja el llit  
**Eines**
  - Espàtula  
  - Alcohol isopropílic o netejavidres (dissolvent base alcohol)  
  - Paper de cuina  

Es recomana netejar el llit calent de tant en tant.  

**Protocol**
1. Desconnectar la impressora del corrent.  
2. Ruixar l'alcohol per la placa i amb el paper de cuina freguem la placa.  
3. Amb l'ajuda de l'espàtula, retirar les restes de laca i antigues impressions.  
4. Repetir el procés si és necessari.

### 5.3.2 Neteja (unclog) de l'extrusor

**Eines**

Per netejar correctament les extrusores de l'impressora, necessiteu el següent:

- Un raspall de filferro
- Un tros de tela
- Una clau Allen número 2 mm i 1,5 mm
- Unes alicates
- Una clau número 7
- Acetona i un bol
- Una agulla.

#### Mètode 1
1. Traieu el conducte i el filament del ventilador

  El primer pas és treure el fan ducte de sota del carro de l’extrusora, després escalfar les extrusores fins a uns 200 graus centígrads i eliminar qualsevol filament que s’hagi pogut utilitzar.


2. Les extrusores estan tapades?
  Escalfar les extrusores també us ajudarà a netejar-les més fàcilment i s’eliminaran més residus de les extrusores quan s’utilitzi el raspall de fil.


 ![Figura 11](./IMG/img11.PNG) 

**Seguretat primer**

  Tingueu en compte l’elevada temperatura de les extrusores quan les netegeu, per tant, per evitar possibles cremades, eviteu el contacte directe de la pell amb la superfície de les extrusores.

No us oblideu dels nozzles

  Quan netegeu les extrusores, també heu de fixar-vos en els nozzles, ja que és important no tenir residus que puguin provocar rascades a l’adhesiu o a la mateixa impressió. Podeu veure els resultats de la neteja de les extrusores i els nozzles.


![Figura 12](./IMG/img12.PNG) 


També podeu provar aquesta opció:

  Si teniu més temps i voleu netejar més les extrusores, també podeu utilitzar un bany d’acetona. Per fer-ho, heu de treure la resistència i el termistor de l'extrusora abans de poder separar-lo completament del carro.

  Primer traieu l'extrusora afluixant el cargol de la part superior.


Extracció del cartutx de calor (resistència)

  Un cop l'extrusora està fora, el següent pas és treure la resistència. Feu això afluixant el cargol amb la clau Allen d'1,5 mm.


![Figura 14](./IMG/img14.PNG) 

Extracció del termistor

  Per treure el termistor, haureu d'utilitzar una clau número 7 i desenganxar-lo per darrere de l'extrusora.


El següent és el bany d’acetona

  Un cop l'extrusora estigui lliure, poseu-la en un bol i aboqueu-hi acetona fins a submergir-la i deixeu-la en remull durant uns 30 minuts. Quan hagin passat 30 minuts, comproveu si teniu algun residu a l’extrusora si no està net, moveu-lo una mica a l’acetona i deixeu-lo per sucar una mica més.

![Figura 15](./IMG/img15.jpg) 


Les extrusores estan netes

  Quan les extrusores i els nozzles estiguin nets, fixareu els termistors i les resistències en aquest ordre. Després podeu tornar a connectar les extrusores al carro, fixar el fan ducte, carregar els filaments extrusors de calor i el llit i començar a imprimir.  

#### Mètode 2
Traieu el conducte del ventilador

  El primer pas és treure el conducte del ventilador de la part inferior del carro de l’extrusora.


Connecteu-vos a Simplify 3D

  Ara connecteu la impressora al programari Simplify3D de l’ordinador i configureu les extrusores per escalfar fins a uns 230-240 graus centígrads. Espereu que la temperatura s’estabilitzi i, a continuació, utilitzeu l’ordre retractar de Simplify3D per provar d’eliminar el filament.


No es pot eliminar el filament amb l'ordre Retract?

  Si no aconsegueixes treure el filament d'aquesta manera, l'altra opció és la següent:


Traieu els nozzles

  Primer heu de treure el nozzle de l'extrusora que està obstruïda. Feu-ho mitjançant les tenalles per mantenir l'extrusora al seu lloc i utilitzeu la clau número 8 per treure el nozzle, tal com es mostra a la imatge següent a la figura 1.


![Figura 28](./IMG/img28.PNG) 


Traieu l'extrusor

  Atenció! Els extrusors encara són molt calents; si us plau, preneu les precaucions necessàries per no provocar lesions.


  El següent pas és treure l’extrusora afluixant el cargol amb la clau Allen número 2 de la part superior dreta de l’extrusora.
 És important mantenir l'extrusora escalfada en aquest punt perquè expandeixi correctament el metall del dissipador de calor, cosa que us permetrà treure l'extrusora.  
 Intenteu fer girar suaument el nozzle cap endavant i cap enrere mentre apliqueu una lleugera pressió cap avall. Si trobeu que no podeu eliminar l'extrusora, no continueu; podeu danyar l’extrusora i el capçal d’impressió.  



![Figura 29](./IMG/img29.PNG) 
  Escalfeu l'extrusora i utilitzeu la clau Allen de 1,5 per desobstruir l'extrusora  
  Després d’extreure l’extrusora, escalfeu-la fins a 240 graus centígrads i utilitzeu les alicates per subjectar-la a l’extrusora per tal de no tenir contacte directe entre la superfície de l’extrusora i la pell.  
  Feu servir la clau Allen número 1.5 per passar a través de l’extrusora fins que el filament que estava enganxat surt. 



![Figura 30](./IMG/img30.PNG) 

  Depenent de com es va trencar el filament, podeu intentar eliminar-lo introduint la clau Allen número 1.5 a banda i banda de l'extrusora.  


Deixeu que l’extrusor es refredi  

  Un cop l'extrusora estigui desobstruïda, podeu deixar que es refredi abans de muntar una còpia de seguretat al carro. També en aquest punt, assegureu-vos de comprovar si hi ha residus al nozzle i feu servir una agulla per comprovar i veure si la ventilació és lliure.  

Ajust de l'alçada de les extrusors  

  Després de muntar l'extrusora, llegiu les instruccions següents de l'enllaç per ajustar l'alçada de l'extrusora.  
### 5.3.3 Neteja extrusor (motor)
1. Traieu la unitat motriu i comenceu traient el motor pas a pas i els passacables:
![Figura 35](./IMG/img35.PNG) 
2. A continuació, traieu el polsador:
![Figura 36](./IMG/img36.PNG) 
3. Ara la unitat d'accionament està desmuntada i es pot netejar:

- Utilitzeu un tros de tela per netejar la roda de pessic a l’eix del motor pas a pas i assegureu-vos d’eliminar els encenalls de filaments;
- també netegeu l'interior del bloc d'alumini on es troba el polsador muntat i assegureu-vos que no hi hagi residus;
![Figura 37](./IMG/img37.PNG) 

4. Un cop netejat tot, torneu a unir la unitat motriu.


- assegureu-vos que la roda de pessic i el coixinet estiguin alineats de manera que el filament no obstrueixi la unitat de transmissió quan utilitzeu la impressora.  
![Figura 38](./IMG/img38.PNG) 

## 5.4 Refrigeració
És important que tots els usuaris d'una impressora 3D FDM sàpiguen utilitzar el ventilador de capa de manera correcta, ja que és un complement que eleva la qualitat i la complexitat de les peces.   

- Ventilador de capa (turbina) 5015  
S'encarrega de refredar el plàstic un cop s'ha dipositat perquè la peça es solidifiqui ràpidament i el plàstic fos no es deformi.  
- Fan hotend 4040  
La seva missió és refrigerar la part freda de l'hotend, és molt important per no cremar el teflon i mantenir una temperatura estable en tot el conjunt i a diferència de l'ventilador de capa SEMPRE ha d'estar connectat.  
- Fan duct (Conducte)  
Ref: https://www.thingiverse.com/thing:1306035  

<br/>

## 6. Resolucio de problemes
### 6.1  Eines
**Eines**
- Joc de claus Allen
- Claus fixes
- Tornavisos
- Espàtula  
- Claus de got  
- Alicates  
******  

### 6.2 Deixa de extruir
  - Filament acabat (substituir bobina de material)
  - Temperatura del "nozzle" baixa (comprovar que la temperatura estigui ben ajustada al sofware o ventilador en mala posició)
  - El filament patina a la roda dentada de l'motor extrusora (retirar filament, tallar fins a la part mossegada i introduir de nou)
  - Hi ha un embús al "nozzle" (substituir "nozzle" o realitzar neteja de "nozzle"
  - Embús a tub PFTE (retirar i substituir)


### 6.3 Mala qualitat en les impressions
  - Alçada de capa molt alta (ajustar en programari)
  - Velocitat elevada (ajustar en programari
  - Mala calibratge (renivelar llit)
  - Poques capes superiors / inferiors (ajustar en programari)
  - Mal ajust del "Slicer"

### 6.4 La peça es desenganxa
 - Peça amb poca adherència a la placa (canviar paràmetres "faldilla, bassa o vora")
 - Utilitzar laca (recomanable Nelly)
 - Manca de material
 - Llit mal anivellada


### 6.5 Per què els meus motors pas a pas de l'eix Y no funcionen? 
**Eines**
  - Claus Allen  

SUGGERIMENT: una manera senzilla de veure quin motor no funciona és fer servir l'ordre Y home d'aquesta manera, els motors es bloquejaran durant aproximadament un minut. Quan s'executa l'ordre, premeu suaument les varetes X situades a l'extrem dret i esquerre i veure quin motor no bloqueja el conjunt.  

Primer gir de la impressora, traieu el tauler lateral amb una clau Allen número 2. i la placa de coberta interior per a la qual necessitareu una clau Allen número 5 i número 3 per accedir a la placa base i comprovar les connexions dels motors pas a pas Y. Com podeu veure a la imatge, els ponts encerclats s'han de muntar en aquesta posició per tal que els dos motors pas a pas Y funcionin correctament Pic.1.  


![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1016055092/original/Capture.PNG?1423132824)

Per tant, els connectors dels dos ponts s'han de muntar d'aquesta manera: el pont número 1 al primer i segon pin i el pont número 2 al segon i tercer pins.  

Assegureu-vos també que la connexió que permet que els motors es moguin sincronitzats entre ells estigui muntada correctament (marcada amb el número 3 a la imatge).  

Si les connexions estan bé, el següent pas és comprovar els motors. Per a això, haureu de treure el tauler posterior de la impressora i desconnectar un motor a la vegada i utilitzar el control Y Jog i veure si un dels motors no funciona:  

- Desconnecteu el motor pas a pas Y esquerre i utilitzeu el control de desplaçament per moure el motor dret i, a continuació, desconnecteu el motor dret i connecteu l'esquerra.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015216041/original/MotorY2.PNG?1421923895)

 Una manera fàcil de comprovar i veure si el problema és el motor o els cables que connecten la placa base amb el motor és treure el motor pas a pas que heu desconnectat i connectar-lo als cables de l’altre motor pas a pas que funciona i veure si d'aquesta manera comença a funcionar, llavors ja sabeu que la causa principal és el motor o els cables .  

### 6.6 Com comprovar la funcionalitat dels motors pas a pas X i Y

 Comprovar els motors pas a pas és molt fàcil i ràpid de fer:  

1. En primer lloc, connecteu el Creatr HS al PC mitjançant el programari que utilitzeu; (Simplifica 3D, materialitza)
2. A continuació, utilitzeu l'ordre home all. Un cop executada aquesta ordre, els motors pas a pas de la impressora es bloquejaran durant 60 segons i no hauríeu de poder moure el capçal d'impressió tant als eixos X com a Y.
3. Proveu de moure el capçal d’impressió als eixos X i Y i comproveu si podeu moure el capçal d’impressió; Si podeu, significa que el motor pas a pas corresponent no funciona correctament.  
**ATENCIÓ !!!! SI ELS MOTORS PAS A PAS NO FUNCIONEN. NO EMPENYEU MOLT EL MUNTATGE SI POSEU MOLTA PRESSIÓ AL MUNTATGE, PODEU DANYAR ELS MOTORS I ELS RODAMENTS!!!!**

[](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1036609080/original/1.PNG?1450682352)

Un motor pas a pas que no funcioni es podria presentar com a problemes següents:

1. Impressions desplaçades a l'eix X o Y
2. Sorolls estranys quan la impressora funciona
3. Moviment esglaonat a l'eix respectiu
4. Resposta als controls de carrera

### 6.7 Com comprovar la funció dels interruptors limitadors (End Stop)

 1. En primer lloc, traieu els 5 perns que mantenen el tauler lateral esquerre al seu lloc, obrint el costat esquerre de la impressora i deixant al descobert el tauler electrònic;  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002358959/original/Capture.PNG?1457423320)


2. A continuació, podeu començar a comprovar els interruptors en un. (vegeu les imatges següents)  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002358982/original/1.png?1457423384)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002358985/original/2.png?1457423391)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002358989/original/3.png?1457423399)


### 6.8 Sota extrusió

Moltes vegades aquest problema és relacionat amb la configuració d'impressió o més específicament amb la configuració d'impressió que es va convertir en una causa mecànica. Això és especialment cert amb els nozzles / blocs de nozzles obstruïts a causa de la configuració incorrecta de la temperatura a la configuració d’impressió del slicer.  

Comprovacions:  

Una altra gran causa de "sota extrusió" és la velocitat d'impressió.  
 L'impressora pot imprimir a 300 mm / s (18000 mm / min) pero no qualsevol model pot imprimir a aquesta velocitat.  
 Els 300 mm / s són molt difícils d’aconseguir: era possible amb una impressió súper bàsica que donava només uns resultats mínims de qualitat de prototipus.  Normalment, cal reduir la velocitat d’impressió en funció de la mida, la complexitat i el gruix de la capa dels models.  
 Si el vostre model té molts canvis direccionals del capçal d’impressió o necessita posar capes més fines que la mitjana (100 micres o menys), us recomanem que reduïu la velocitat (penseu en un tub modificat que intenti prémer la pasta de dents amb precisió sobre un raspall) ... pasta de dents a tot arreu).  
 En cas contrari, podríeu experimentar una extrusió a causa de l’alta velocitat. Com provar si la velocitat està provocant l'extrusió inferior?  
 Proveu de reduir-lo fins a 60 mm / s (3600 mm / min) i comproveu si encara us extrudeu: no hi ha cap problema: la velocitat és la culpable.   
 I us ho direm ara: sort amb impressions amb èxit fins i tot a aquesta velocitat si la vostra part és una sèrie de punxes fines, torres, columnes o parets molt fines.  

No obstant això, aquest article es centra a proporcionar explicacions sobre algunes de les següents coses que cal comprovar a la màquina, en cas que trobeu que totes les temperatures de configuració de la impressió són correctes, heu de comprovar immediatament si l'extrusora està obstruïda. Us recomanem que carregueu filament PLA a l'extrusora qüestionable, preescalfeu la impressora (per PLA, connecteu-la a l'ordinador i preescalfeu-la a 240º) i extrudeu manualment algun filament.  

Això és bàsicament com hauria de ser la unitat de transmissió (la imatge es despulla a propòsit per proporcionar-vos la millor vista, de dalt a baix):  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006533431/original/HS%20-%20drive%20unit%20-%20ideal%20alignment%20-%20diagram.PNG?1467788101)

L'anell de pressió (A) a la part posterior de cada unitat d'accionament del filament controla la pressió que la roda de pressió  aplica sobre el filament a l'engranatge d'accionament creant una bona alineació i pressió al punt D de la imatge. El cercle taronja representa el filament.  

No obstant això, aquí teniu alguns problemes que hem vist que poden causar en extrusió:  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006533598/original/HS%20-%20drive%20unit%20-%20pressure%20ring%20too%20loose%20-%20diagram.PNG?1467788397)

#### Anell de pressió massa fluix  

Aquí veieu on l’anell de pressió està massa fluix provocant un buit (D) entre la roda de pessic (B) i l’engranatge motriu (C). El resultat serà que el filament rellisqui, provocant un so de clic molt notable quan s’intenta extruir. El filament es mou amb força facilitat en aquest cas.  

Per resoldre-ho, estreneu-lo a mà (MAI USEU EINES, ja que això pot doblegar l’eix entre l’anell de pressió i la roda de pressió) l’anell de pressió (A) fins que només vegeu aproximadament un o dos rosques de cargol al punt A.1.  

Això crearà més pressió entre la roda de pessic i l’engranatge motriu, pressionant el filament contra les dents de l’engranatge motriu.  

Us adjuntem un vídeo que mostra com podeu provar si l'anell de pressió pot estar massa fluix; el vídeo mostra que algú pot fer que el filament es torni a alimentar a la unitat de transmissió aplicant resistència.  

Vegeu també "Gear Gear is Malalined", ja que els símptomes poden ser similars.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006533782/original/HS%20-%20drive%20unit%20-%20pressure%20ring%20too%20tight-%20diagram.PNG?1467788808)

#### L'anell de pressió és massa ajustat

A la imatge de la dreta, veureu que l'anell de pressió (A) està cargolat gairebé completament a la part posterior de la impressora fent que el buit (D) es tanqui massa fort danyant el filament. Aquesta petita distància entre la roda de pessic (B) i l’engranatge motriu (C) pot causar una extrusió en danyar el propi filament.  

El programari i el microprogramari per tallar estan configurats perquè un filament d’1,75 mm s’alimenti a través de la màquina. Tanmateix, si envieu filaments que contenen escletxes i trossos, tot i que l'extrusora, s'extrudirà, ja que el filament ja no fa 1,75 mm.  
 
A més, hem vist filaments amb certs trencaments que han provocat que el filament s’enganxi a la unitat de transmissió, principalment allà on s’alimenta a través del filament situat a la part superior de la unitat de transmissió.  

Finalment, se sap que el residu de filament que queda a les dents de l’engranatge motriu fa que rellisqui el filament, ja que les dents de l’engranatge motriu ja no poden agafar el filament per alimentar-lo.  

Per solucionar-ho, afluixeu l'anell de pressió (A) i talleu el filament danyat.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006535671/original/Filament%20gashes.png?1467792437)


#### Osques al filament

NOTA: Tot i que la pressió pot ser massa o massa petita, hi ha una mica de joc en la pressió "perfecta" (és a dir, no ha de ser exacta). La raó per la qual fins i tot es pot ajustar és ajudar els clients a proporcionar-los una mica més de facilitat en utilitzar el comandament d'alliberament ràpid quan carreguen i descarreguin filaments. Aquest és el motiu pel qual és possible que hagueu rebut una impressora amb ella no configurada perfectament; és possible que s’hagi ajustat a l’hora de fer el control de qualitat a la fàbrica.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006535540/original/HS%20-%20drive%20unit%20-%20Drive%20Gear%20not%20aligned%20-%20diagram.PNG?1467792174)

#### L'engranatge de transmissió està desalineat

A l’esquerra, veieu un problema en què l’anell de pressió (A) i les rosques (A.1) tenen bon aspecte, però el filament rellisca (normalment a intervals irregulars) perquè l’engranatge de transmissió (C) no està alineat amb el pessic roda (B).

Això provoca una pèrdua aleatòria de velocitat d’extrusió i pot provocar que el filament s’extreu realment del costat de la unitat de transmissió a través de l’espai lateral creat (D).

Una vegada més, és possible que escolteu el so del clic del filament saltant, fins i tot si l'anell de pressió està perfectament configurat. Els símptomes poden ser similars als de l'anell de pressió massa fluix.

Per resoldre aquest problema, utilitzeu una clau Allen / hexagonal / controlador número dos per afluixar el cargol de fixació de l'engranatge de transmissió (el petit cercle negre de l'engranatge de transmissió (C) de la imatge a l'esquerra) i centreu l'engranatge de transmissió. Si la pressió sobre la roda de pessic és prou forta, es pot lliscar l’engranatge de transmissió fluix i sentiràs que les cantonades de la roda de pessic s’enfonsen; troba el mitjà feliç. Finalment, mantingueu l’engranatge de transmissió al seu lloc i torneu a apretar el cargol.

Podeu obtenir més informació al respecte al pas 4 de l'article següent sobre la solució:

![](http://support.lpfrg.com/support/solutions/articles/1000206350-how-to-clean-the-drive-units-of-my-creatr-hs-3d-printer)


![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006536264/original/HS%20-%20drive%20unit%20-%20extremely%20rare%20broken%20motor%20shaft%20-Wobbles-%20diagram.PNG?1467793836)

#### Eix del motor de l'extrusora danyat

Tot i que això no és un problema habitual, és possible que l'anell de pressió (A) i la rosca (A.1) provoquin que la roda de pessic (B) creï una bona pressió sobre l'engranatge de transmissió (C), però tenint problemes en què l'extrusió va i ve en el que sembla ones regulars.  

Hem observat en alguns casos que l'eix del motor del motor pas a pas de l'extrusora està doblegat o danyat, cosa que fa que trontolli quan giri. Aquesta oscil·lació lenta fa que el filament salti a intervals regulars. Pot ni tan sols saltar-se, sinó simplement extruir una mica menys en alguns punts i no en altres.  

Això pot ser difícil de detectar mitjançant una extrusió de control de moviment o una ordre de càrrega de filament. Sovint es veu a la primera capa de les primeres grans capes on s’imprimeix (vegeu la imatge següent).  

La millor manera de confirmar-ho és treure la unitat de transmissió de la impressora i girar l'eix del motor amb els dits (observant l'eix per si es balanceja).  


![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006537343/original/broken%20motor%20axle%20print%20results.PNG?1467795560)

**Un eix del motor doblegat / trencat sol imprimir-se en tires que semblen força espaiades**

### 6.9 Per què s'imprimeix a l'aire després de poques capes?

1. Primer atureu la impressora i comproveu les bobines de filament i comproveu si el filament es va trencar.  Si el filament està intacte, traieu el carret i comproveu que, a causa de l’enrotllament, el filament s’enrotlla al tub o hi ha una superposició de filament al carret.  

SUGGERIMENT: quan la bobina del filament és nova per tal d'evitar que es superposin, podeu col·locar-la davant de la impressora tal com es pot veure a la imatge següent.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015413950/original/IMG_20150119_084005.jpg?1422278881)

2. Una altra opció és que la pressió sobre el filament sigui suficient o insuficient i que el filament rellisqui entre l’engranatge motriu i el coixinet. Per ajustar la pressió, utilitzeu el comandament de la unitat motriu i feu més o menys pressió sobre el filament segons els casos;  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024383417/original/IMG_20150115_083858.jpg?1434349655)

També mireu l'aspecte del filament a la unitat de transmissió. Ha d’estar recta a l’engranatge de transmissió, com a la imatge següent;  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024383659/original/IMG_20150507_144551.jpg?1434349917)

3. Si el filament està bé, comproveu si l'extrusora està obstruïda. Per fer-ho, ordeneu a la impressora que extrusi entre 50-70 mm de filament i vegeu si l'extrusió és constant. Si no surt cap filament, seguiu  i consulteu com desactivar els extrusors .  

4. També si el material extruït és inusualment prim, traieu els noozles, extrudeu uns 40-50 mm de material sense els noozles i comproveu si l'extrusió és adequada. Un cop feta l'extrusió si la mida del filament és constant, els nozzles estan tapats i cal substituir-los .  

Per evitar futures extrusores obstruïdes, seguiu i vegeu com netejar correctament l'extrusora i els nozzles.  

### 6.10 Sobreextrusió

Avui dia gairebé no rebem casos d’extrusió excessiva, però el suport tècnic li encanta proporcionar informació. Per tant, vam decidir escriure aquest article de solució ràpida per si sou un dels rars casos en què sembla que s’extreu massa filament.  

**Causes de la configuració d'impressió**

#### La temperatura és massa alta 

Alguns filaments simplement se sotmetran a piròlisi (es converteixen en carboni, també conegut com "coure") quan la temperatura de l'extrusora és massa alta (cosa que podria provocar l'extrusió amb un embussament), mentre que d'altres poden filtrar-se excessivament, provocant una extrusió excessiva.  
Verifiqueu que la configuració de la temperatura estigui configurada correctament als perfils d’impressió que utilitzeu. Aquí teniu alguns números:  

**PLA**

* Extrusor ► 195º-220º
* Llit ►40º-60º (2% de les capes; alguns clients fins i tot preescalfen la impressora i després executen el .GCODE sense ajustar la temperatura del llit.)

#### Multiplicador d'extrusió (Simplify3D) / Factor d'extrusió massiva

Per a aquests paràmetres, voleu que el mantingueu el més proper al 100% (o 1.0 a Simplify3D) com sigui possible, ja que els nostres programadors ajusten el firmware per produir impressions excel·lents a aquest valor.  
Només hi ha ocasions molt rares en què us plantegeu passar per sobre del 100% (1,0), però mai per sobre del 140% (1,4).

#### Qualsevol de la configuració d'extrusió de subcategoria superior al 100%

Tingueu en compte que, tot i que hi ha un multiplicador d’extrusió global, també hi ha multiplicadors d’extrusió de subcategories.  
El programari Creatr, per exemple, us permet ajustar les extrusions per a la configuració de contorns, omplert i pell de pell / baixada (capes superior i inferior).  
Tot i que és bastant normal configurar el recanvi al 150% d’extrusió, normalment no es recomana tornar-se boig amb aquests paràmetres. Si teniu una extrusió excessiva, comproveu també la configuració en aquestes subcategories.  

#### Configuració dels nozzles i configuració de l’amplada del fil dels nozzles

Assegureu-vos que la configuració dels nozzles sigui de 0,4 mm si teniu el nozzle estàndard de 0,4 mm que inclou la impressora . A Simplify3D, també hi ha una configuració anomenada Amplada d’extrusió (pestanya Extrusora) que s’ha d’establir a 0,40 mm:

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006544800/original/s3d%20over%20extrusion.PNG?1467809115)
  

Hi ha moltes causes mecàniques per a l’extrusió.

#### Nozzle danyat

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11006543470/original/WP_20160617_14_13_50_Pro.jpg?1467807153)

A l’esquerra, a la imatge superior, podeu veure una extrusió normal amb un nou bloc de nozzles. A la dreta, podeu veure una extrusió amb la mateixa configuració exacta només amb un nozzle trencat. Fixeu-vos en la diferència d’amplades d’extrusió. No hi ha cap altra solució que la substitució del bloc de nozzles.  

Fer malbé els nozzles és una de les causes més freqüents que es veu al suport tècnic.  
Això es pot fer substituint un nozzle, alineant-lo massa baix al capçal d’impressió i, a continuació, col·loqueu el vidre que premeu cap als nozzles.  
Altres causes han estat l’ús d’eines de neteja dures amb massa freqüència, com ara un raspall de filferro. La causa final d’una punta de nozzle danyada que hem vist són els clients que utilitzen un tipus de filament abrasiu que no és compatible amb la impressora Creatr HS (com el tipus de filament de carboni), que desgastarà les puntes dels nozzles de llautó molt ràpidament deixant un orifici de la tovera. és més gran que 0,4 mm.  

#### Cal actualitzar el el firmware

Tot i que això en el passat ha estat realment una causa de la extrusió, és possible que la vostra impressora simplement s’hagi d’actualitzar. Per actualitzar el programari i el el firmware a Creatr HS, consulteu els tres enllaços següents; s'han de fer en l'ordre que hem inclòs aquí:


#### Cal modificar el firmware

Les extrusores funcionen amb els mateixos motors pas a pas que els eixos X i Y. Hem vist alguns (i volem dir MOLT pocs) problemes en què aquests motors han necessitat de modificar una mica els passos del firmware de la impressora. Això és certament possible.  


####  Dimensió incorrecta del filament

Aquestes impressores només funcionen amb filament de 1,75 mm. Si carregueu una altra cosa, és possible que el programa de tall de 3D no pugui calcular l'extrusió adequada per a això.  
* * *


### Funcionament del sensor de temperatura d'una impressora 3D

![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002951486/original/HS%20thermistor%20-%20NTC.PNG?1458823506)  


Un termistor és una resistència elèctrica que varia el seu valor en funció de la temperatura. En les impressores 3D, s'utilizen del tipus NTC(Negative Temperature Coefficient). Això significa que com més alta sigui la temperatura del termistor, menor serà el valor la resistència (mesurada en ohms, "Ω"). Quan s’escalfa, el firmware utilitza aquesta lectura per estabilitzar la temperatura d'extrusio a una temperatura determinada.

En cas d'error en la lectura, la impressora entra un mode de bloqueig que anomenem MIN / MAX Temp Triggered Error. En aquest cas, es necessari canviar el sensor.
En el cas de les impresores Leapfrog Creatr HS es necessari disposar d'ambos sensors conectats per evitar aquest tipus d'error.


**Solució**

La manera més senzilla de saber si pot tenir un problema de termistor al Creatr HS és mirar les lectures de temperatura a la pantalla frontal ABANS de preescalfar la impressora:

![Figura 38](./IMG/img24cat.png)


El número esquerre dels 3 parells MAI hauria de llegir 0º (tret que la impressora estigui fora i estigui per sota de zero). Per tant, si veieu 0/0 per algun d’aquests 3 números, sabreu que hi ha un problema amb un termistor O el cable de cinta que passa per la cadena d’arrossegament (que retransmet les lectures del termistor). De la mateixa manera, en qualsevol impressora que es connecti al PC, podeu comprovar la lectura de temperatura al programa de tall de 3D.  

En aquest punt, voldreu determinar si es tracta del termistor o del cable de cinta (un cable de cinta danyat pot desordenar el senyal d’un termistor perfectament bo).  

És possible que hàgueu tingut un punt d’impressió a mitja impressió encara que actualment llegiu bones lectures ambientals en els 3 termistors. Amb l’encès, podeu moure lliurement el capçal d’impressió amb la mà; fes-ho per les 4 cantonades que moguin el cable de cinta a la cadena d’arrossegament mentre observes les lectures de temperatura ambient a la pantalla del tauler frontal. Si hi ha un curt en aquest cable, les lectures es poden llegir quan el capçal d’impressió es troba en una posició i no es poden llegir quan el capçal d’impressió és una altra posició. Si ho observeu, estem gairebé segurs que es tracta d’un error de desencadenament temporal MIN / MAX causat a un _cable de cinta danyat_. Aquí teniu un petit vídeo que mostra aquesta prova:  

Si ho observeu, la lectura de l'extrusora dreta a la pantalla comença a mostrar el 44º / 0 (la impressora s'ha escalfat temporalment abans de fer aquest vídeo). Tot i això, només movent una mica el capçal d’impressió, el filferro de la cadena d’arrossegament es queda curt i fa que la lectura del termistor baixi a 0/0. De nou, això, sens dubte, és un error de desencadenament de temperatura MIN / MAX causat per un cable de cinta danyat a l'interior de la cadena d'arrossegament.  

**Llit escalfat?** Per comprovar el termistor del llit escalfat, col·loqueu la impressora a casa i remeneu els cables que provenen del llit escalfat i condueixen a la placa base. Si es produeix el mateix tipus de curt que es descriu anteriorment (la temperatura del llit canvia sobtadament a 0/0 quan els cables estan lleugerament doblegats), ja sabeu que és el termistor del llit escalfat.  

Si veieu 0/0, la comprovació anterior pot no funcionar. Per tant, traieu els panells laterals de la impressora i la placa de protecció contra pols per deixar al descobert la placa base. Canvieu la connexió de la placa base del termistor que està llegint malament amb una connexió diferent de la placa base del termistor:

![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002908747/original/HS%20-%20Motherboard%20-%20Thermistor%20Connections.PNG?1458734743)  


Si el termistor o el cable de cinta estan malament, la lectura 0/0 a la pantalla del tauler frontal hauria de seguir on l'heu connectat. Per exemple, si l'extrusor esquerre mostra originalment 0/0 i canvieu la connexió de cable VERD / BLAU amb la connexió ORANGE / GROC, lògicament l'extrusora dreta hauria de mostrar una lectura de 0/0 (pot ser que calgui reiniciar la impressora). SI la lectura 0/0 es manté a la part esquerra (en aquest exemple), el problema és realment amb la placa base.  

Finalment, si creieu que és el termistor, podeu desconnectar el termistor del capçal d’impressió, descargolar-lo del bloc de l’extrusora i connectar-lo a la placa base directament (passant per alt el cable de la cinta i la PCB del capçal d’impressió).  

Heu d’assegurar-vos que els 3 termistors estan connectats a la placa base, així que assegureu-vos de desconnectar el termistor de cable de la cinta corresponent en connectar-lo directament a la placa base. Per exemple, si sospiteu que el termistor de l'extrusora adequat està causant el problema, traieu-lo del capçal d'impressió i connecteu-lo directament a la ranura a la qual està connectat el cable TARONJA / GROC:  

![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002951620/original/HS%20-%20Thermistor%20check%20-%20direct%20motherboard%20connection%20test.PNG?1458823710)  


A continuació, apagueu la impressora i torneu-la a engegar (s'enviarà l'ordre M999 que restableix la impressora i la treu del bloqueig SI el termistor no es fa malbé). De nou, un termistor danyat mostrarà un 0/0 a la pantalla quan es connecti a la placa base així.  

  

Aquí teniu una lectura de Simpliy3D que mostra que el termistor adequat llegeix bé la temperatura ambiental quan vam provar de connectar el termistor directament a la placa base:  


(La lectura de temperatura indicada en groc és la lectura del termistor quan es connecta el termistor directament a la connexió correcta de la placa base del termistor. La lectura de temperatura indicada amb la caixa taronja és la lectura de temperatura ambiental del llit escalfat.)  

Si obteniu una lectura 0/0 amb el termistor connectat directament a la placa base i l’heu provat mitjançant el mètode CHECK 2, estem segurs que cal substituir el termistor.  

**D'acord ... Així que el capçal d'impressió s'ha aturat i tots els termistors (i el cable de cinta) es veuen bé ... Ara què?**

En un programari de tall en 3D que té un terminal, algun moment un error que diu "El firmware no respon". Si veieu aquest error o heu fet això i heu constatat que tots els termistors funcionen bé.  

### 6.11 Calibratge del llit impossible
En algun moment podem trobar que tenim el llit molt estret a la part del davant i molt fluixa al darrere i per molt que es calibri no queda bé, aquest problema pot venir de les torretes que subjecten el llit (eix z), per algun motiu una torreta aquesta més alta que una altra i això ens impedeix realitzar una calibracions correctament.

1. El primer pas és desconnectar la impressora, destensar i retirar la corretja de l'eix z, serà més fàcil si retirem la tapa lateral.
![Figura](./IMG/torretas.jpg)

2. Amb un nivell posar-lo sobre el costat afectat o comprovar-los tots, i assegurar-se que la bombolla queda en el centre la foto següent és d'una descalibrada.
![Figura](./IMG/nivel.jpg)

3. Moure manualment les torretes fins que quedi a nivell tota la plataforma del llit

4. Tornar a posar la corretja en el seu lloc, amb molta cura que mentre la posem es moguin totes les torres alhora fins a encaixar-la de nou.

5. Tornar a tibar la Corretja

### 6.12 Un dels extrusors no escalfa
Ens hem trobat en una ocasió una maquina, que deixo d'escalfar una dels filtres. Els símptomes van ser que:
- La impressora marca la temperatura correcta en tots dos filtres, però a, escalfar els dos, només un puja de temperatura.

![Figura](./IMG/Imagen29.png)

Com veiem en la imatge les dues sondes tenen temperatura ambient
Passos a seguir per a trobar l'avaria
1. Revisar el cablejat des de la placa fins al filtre, per a això s'haurà de desmuntar el capçal de la impressora per complet

- En aquest cas, la placa electrònica del capçal (On es posen els escalfadors) la peça que subjecta els cables de l'escalfador s'hi havia des-soldat (Soldadura de fabrica molt feble) i va fer que al mínim moviment es trenqués la soldadura


- Solució
Soldar de nou la peça amb estany abundant ja que la soldadura és bastant gran, muntar i provar.

Observació (Una vegada realitzat aquest procés, és gairebé obligatori realitzar un "pid tuning" atès que el cartutx pot estar descalibrat i les temperatures poden superar els >300 °C)

-Com realitzar un pid tunning (https://all3dp.com/2/3d-printer-pid-tuning/)


## 7. Substituir peces

### 7.1 Com canviar els ventiladors del capçal

1. El primer pas és aturar la impressora i treure els tubs de filament del capçal d’impressió i el fanducte de la ranura situada sota el capçal d’impressió. A continuació, talleu el suport que manté la cadena d'arrossegament al lloc de la imatge 1 i desconnecteu l'extrem de la cadena d'arrossegament com podeu veure a la imatge 2.  

![Figura](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1014984137/original/Capture.PNG?1421664580)
![Figura](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1014982307/original/DragChain.PNG?1421663876)

2. El segon pas és descargolar el cargol frontal situat al capçal d’impressió mitjançant la clau Allen número 2.5 i treure la carcassa del capçal d’impressió com es pot veure a la figura 3.  

![Figura 38](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015410820/original/Capture2.PNG?1422276122)  

3. Un cop traieu la carcassa, tindreu accés a totes les connexions del capçal d’impressió (ventilador, termistor, connexions de resistència). Si voleu substituir el ventilador situat a la part superior del capçal d'impressió, utilitzeu la clau Allen número 3 per descargolar-lo i, a continuació, desconnecteu-lo del capçal d'impressió. Podeu veure quina connexió és per al ventilador superior  o simplement seguir els cables del ventilador. El connector número 1 és per al ventilador inferior i el connector número 2 per al ventilador superior.  

![Figura](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015411221/original/Capture1.PNG?1422276484)

4. Si el ventilador inferior és el que no funciona, desconnecteu-lo del capçal d’impressió i, a continuació, utilitzeu un tornavís de cap pla per descargolar-lo del capçal d’impressió, tal com es mostra a la figura 5.  

![Figura](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015405937/original/Capture.PNG?1422271750)

6. Després de treure el ventilador defectuós, agafeu la substitució, torneu-la a fixar al capçal d'impressió i, a continuació, connecteu-la. Un cop hàgiu fet i comprovat que les connexions estan bé, podeu tornar a muntar la carcassa del capçal d'impressió.  

Tampoc no us oblideu de tornar a col·locar el fanducte i els tubs de filament al lloc que els correspon.  


### 7.2 Com substituir el llit 
**Eines**

- Una clau anglesa número 10  
- Una clau Allen número 2, 3 i 5  
- Un parell de tenalles  
- 3 embolcalls de corbata    


1. Descomposició del llit escalfat.  
   Apagar la impressora. A continuació, amb la clau número 10, traieu les 4 femelles que mantenen el llit escalfat al lloc.

2. Assegureu-vos de fer un seguiment de totes les femelles i cargols que traureu. També en aquest punt heu de treure el feltre i els panells laterals drets amb la clau Allen número 2.

3. Després d’extreure les 4 femelles i els panells laterals, utilitzeu les alicates de tall per treure els 3 embolcalls de llaç que hi ha a la imatge de la imatge 2.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015890672/original/IMG_20150203_091513.jpg?1422954353)

4. Després de treure els embolcalls de la corbata, el següent és treure el cable enrotllat negre fins a la placa de coberta interior i desconnectar els cables del llit escalfat els 2 connectors blaus indicats a la imatge anterior per les 2 fletxes.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002979712/original/sa%20step%2003.PNG?1458894146)

5. Extracció de la placa de coberta interior:

  Un cop desconnectats tots els cables, és hora de retirar la placa de coberta interior. Primer utilitzant la clau Allen número 3, traieu els tres parabolts encerclats a la imatge de sota de la imatge 3  

6. A continuació, amb la clau Allen número 5, afluixeu els cargols laterals de la placa de tapa perquè pugueu treure-la Pic. 4. Tingueu cura de no desbloquejar-los i, a continuació, només afluixeu-los una mica.  

Extracció del termistor:

  El darrer pas per treure el vell llit escalfat és desconnectar el termistor de la placa base. Per fer-ho, desconnecteu el cable enrotllat negre fins que el pugueu treure i, a continuació, desconnecteu el termistor encerclat a la imatge de sota de la imatge 5.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1015892834/original/Capture.PNG?1422956408)

  Després de treure el termistor, és segur retirar el vell llit escalfat i posar-hi el nou. Per muntar correctament el nou llit climatitzat, seguiu aquests passos en l’altre ordre en què es van presentar des de l’últim fins al primer.  

**ATENCIÓ!**

**Quan connecteu els cables del nou llit escalfat, assegureu-vos que els connectors estiguin empesos fins a evitar el contacte no desitjat entre ells que pugui provocar un curtcircuit.**

**Quan hàgiu acabat de substituir el llit climatitzat, inicieu la impressora i calibreu-la (instruccions a l'enllaç següent) escalfeu-la i comenceu a imprimir.**

![](http://support.lpfrg.com/solution/articles/1000198012-how-to-calibrate-your-creatr-hs-heated-bed-instruction-video)

ACTUALITZACIÓ: Recentment hem començat a produir llits climatitzats amb un cable de termistor reduït i a instal·lar una extensió per al termistor a l'extensió de l'element calefactor, tal com es veu a la imatge següent:

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002855343/original/HS%20-%20heated%20bed%20wire%20extension.PNG?1458638328)

Si heu rebut un llit escalfat amb un cable curt de termistor, també hauríeu d’haver rebut una de les extensions de la imatge anterior. Per tant, el PAS 2 anterior pot ser una mica diferent.

****
### 7.3 Com substituir els coixinets de la impressora
**Eines**

- Claus allen
- Una clau anglesa número 7  
<br>
1. El primer que heu de fer és apagar la impressora i, a continuació, traieu el panell lateral esquerre i dret de la impressora i el panell posterior.  
2. Després, amb una clau Allen número 5, traieu la tapa superior.  
3. Després d’eliminar-lo, el següent pas és alliberar les varetes X deixant anar els cargols de fixació del conjunt que manté la tensió a la corretja X i també manté les varetes X al seu lloc (vegeu la imatge següent). Primer utilitzeu una clau número 7 per afluixar la femella i, a continuació, afluixeu els cargols amb una clau Allen número 2.  
4. Quan les barres X estiguin soltes, tireu-les cap al costat dret porteu el llit fins a les extrusores i recolzeu-les al llit.  
5. Ara podeu treure el conjunt Y desbloquejant els cargols que el subjecten al marc de la impressora mitjançant una clau Allen número 4.  
7. Ara, traieu tot el conjunt i afluixeu la corretja descargolant les taques del suport frontal i, a continuació, podeu descargolar la vareta.  
8. Ara, l’últim pas és treure els coixinets de la corretja. Per fer-ho, utilitzeu un tros de filferro i introduïu-lo a la carcassa per mantenir la femella al seu lloc mentre utilitzeu una clau Allen número 2 per treure el suport que subjecta la corretja.  
9. Un cop el suport estigui retirat, traieu els rodaments trencats de la vareta i substituïu-los per uns de nous.  
10. Tingueu en compte també que els nous coixinets ja estan lubricats, de manera que només cal muntar-los.  

ATENCIÓ: Assegureu-vos, quan torneu a muntar la corretja a la caixa del coixinet, que estigui centrada al centre del suport que la manté al seu lloc.  

Un cop la carcassa i els coixinets antics estiguin fora, canvieu-los amb les peces de recanvi i torneu-les a unir tot resseguint els passos per separar-los.  

****

### 7.4 Substitució del conjunt electrònic de la impressora

**Eines**
- una clau Allen número 2,5, 3, 4 i 5  
- un tornavís de cap estrella i un tornavís de cap pla  

1. El primer pas és apagar la impressora i treure l’endoll per evitar que hi hagi corrent elèctric pel sistema.
2. Un cop desconnectat, utilitzeu la clau Allen número 2.5 i traieu els panells esquerre i dret (cadascun té 5 cargols Pic.1).
3. Ara heu de treure la placa de coberta interior. Per eliminar-lo, heu de treure 7 parabolts Pic.2:

 - Dos d'ells amb la clau Allen número 5 (una a la dreta i una a l'esquerra);
 - Tres parabolts amb la clau Allen número 3 i dos amb el número 4.

4. Ara que s'ha tapat la tapa, assegureu-vos de treure els tubs de guia del filament de l'empenta dels acoblaments i tireu-los cap a un costat (per treure els tubs amb una sola mà premeu la vora de l'empenta en acoblament i feu lliscar el tub cap a fora).

5. Ara que els tubs estan fora del camí, el següent pas és treure tots els connectors de la placa base Pic.5 i Pic.6:

6. Un cop tots els connectors estiguin fora, obtindreu accés a la placa OLIMEX antiga i haureu de desconnectar els cables següents (vegeu la imatge següent) Imatge 7:

(així es connecta la placa antiga)

7. Ara l'última part que heu de desconnectar són 2 cables:

8. El primer és el cable que connecta la font d'alimentació a l'endoll (utilitzeu el tornavís de cap estrella) Imatge 8:

 - i per a l'últim cable, utilitzeu el tornavís Star Head per treure-lo de la PSU i, a continuació, utilitzeu el tornavís de cap pla i desconnecteu els ventiladors que refreden les dues plaques del connector que passen a la placa base Pic.9 i Pic.10;


8. Un cop tots els connectors estiguin fora, és hora de retirar el conjunt:

- L’assemblea es manté al seu lloc mitjançant set tacs de plàstic 3 a la part frontal i 4 a la part posterior Pic.11:

9. Només cal prémer els tacs amb una mà i aixecar el conjunt amb l'altra.

Un cop traieu el conjunt vell, agafeu el nou i monteu-lo als tacs de plàstic i, a continuació, comenceu a connectar els cables de la manera següent:

10. Torneu a connectar els cables d’alimentació a la PSU i els ventiladors que refreden les plaques al connector del nou conjunt (vegeu les imatges 8, 9 i 10).

 - Ara traieu la placa base del conjunt i comenceu a connectar la placa olimex (vegeu la imatge següent) Fig.12;

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1020564093/original/IMG_20150420_143928.jpg?1429610072)
11. La connexió marcada amb 1 és el botó de visualització;

- La connexió marcada amb 2 és el xip FTDI que es connecta al cable USB que condueix a la part posterior de la impressora i que s’utilitza per a la comunicació entre el PC i la impressora;

- La connexió marcada amb 3 és la pantalla del HS.

També hi ha dos cables USB que s’han de connectar a l’OLIMEX: - un és el cable USB del port USB de la pantalla i l’altre és el cable USB tipus AB que s’utilitza per a la comunicació entre l’OLIMEX i la placa base.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1020564651/original/IMG_20150420_143935.jpg?1429610476)

Ara l’OLIMEX està connectat i podeu passar a la placa base:  

12. Torneu a posar el tauler al seu lloc i comenceu a connectar els cables:  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1020565061/original/IMG_20150416_154127.jpg?1429610733)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1035987397/original/HS%20-%20Motherboard%20-%20motor%20cable%20connection%20-%20motor%20power%20cable.PNG?1449751756)

Ara que es fan les connexions de la placa base, feu una comprovació final i, a continuació, endolleu la impressora, arrenceu-la i comproveu el funcionament de la pantalla i, quan us assegureu que tot funciona, podeu muntar la placa de coberta interior i els panells laterals.  


### 7.5 Com substituir el subconjunt del cable de la cinta
**Eines**
- Una clau Allen número 2 i un número 2,5  
- Un tornavís de cap pla.  

1. Primer apagueu la impressora i traieu el cable d'alimentació de l'endoll de la impressora.  
2. En segon lloc, traieu el suport de muntatge que manté la cadena d’arrossegament al seu lloc (vegeu la figura 1) i, a continuació, traieu la cadena d’arrossegament

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022735911/original/Capture.PNG?1432214046)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022735943/original/Capture.PNG?1432214078)

3. Ara utilitzeu la clau Allen de 2,5 i traieu la carcassa del capçal d’impressió per accedir a on estan connectats els cables al capçal d’impressió (imatge ?).

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022736117/original/Capture.PNG?1432214186)

4. Ara traieu el connector (imatge ?) dels cables perquè pugueu començar a treballar fins a la placa base.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022818620/original/IMG_20150522_104903.jpg?1432281298)

5. Després de desconnectar els cables, podeu eliminar la següent cadena d'arrossegament negre

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022834631/original/2.PNG?1432295131)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022834654/original/Capture.PNG?1432295171)

6. Ara podeu desconnectar els cables de la placa base (imatge ?):

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022834738/original/4.JPG?1432295242)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022834770/original/5.JPG?1432295282)

7. Un cop desconnectats els cables, traieu el cable enrotllat negre (imatge ?) que subjecta els cables i comenceu a recórrer els passos i connecteu els cables de recanvi respectant el codi de color que podeu veure a les imatges d'aquesta solució.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1022834946/original/1.JPG?1432295483)

8. Un cop hàgiu connectat els cables i torneu a col·locar-los tot al seu lloc, podreu tornar a iniciar la impressora i començar a imprimir.


### 7.6 Com substituir el PCB de la vostra impressora Creatr HS
* * *

**Eines**
Per als passos que haureu de fer per canviar el PCB, necessitareu el següent:

- Petit tornavís de ranura
- 2.5 Clau Allen


**Traieu la tapa superior**

1. Primer heu de descargolar el cargol frontal (imatge ?) amb la clau Allen de 2,5.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024644583/original/IMG_0071.JPG?143461355)

2. Després que la part frontal de la coberta estigui solta, traieu els 4 cargols posteriors  amb la clau Allen de 2,5.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024644955/original/IMG_0061.JPG?1434613846)

3. Després de retirar tots els cargols, aixequeu la part frontal de la coberta empenyent la part posterior cap avall .  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024647087/original/IMG_0058.JPG?1434615663)

 **Desconnecteu tots els cables i substituïu el PCB**

4. Un cop la tapa estigui solta i tingueu accés a la placa i els cables, desconnecteu-los tots (imatge ?). Per desconnectar el cartutx de l’escalfador, necessitareu un petit tornavís de ranura.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024648096/original/IMG_0063.JPG?1434616663)

5. Després de desconnectar tots els cables, amb la clau Allen 2.5, descargoleu els 3 perns que subjecten el PCB (imatge ?) i substituïu-lo per la nova placa.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024648463/original/IMG_0066.JPG?1434616930)

6. Tingueu cura de no perdre els 3 separadors entre el PCB i la placa metàl·lica. (foto 6)  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024648845/original/IMG_0067.JPG?1434617268)


**Torneu a muntar la tapa superior**

7. Després que el nou PCB estigui al seu lloc, torneu a connectar tots els cables a les seves ranures () foto 4 ) torneu els 4 cargols posteriors amb la clau de rosca 2,5 (tingueu cura que el cable de cinta s’ha d’inserir primer a la ranura ( Pic.7 )

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1024653298/original/IMG_0069.JPG?1434620305)

Un cop el cable de la cinta estigui al seu lloc, feu el moviment invers i, a continuació, cargoleu el pern frontal.
  
### 7.7 Com substituir les unitats d'unitat de filament al Creatr HS

**Eines**

- Clau Allen de 2,5.

* * *

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949739/original/sa%20step%2001.PNG?1458819856)

1. Desconnecteu el cable d'alimentació de la impressora i traieu el tauler posterior de la impressora:

   
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1030255801/original/1.PNG?1442218946)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949751/original/sa%20step%2002.PNG?1458819901)

2. Traieu l’empenta dels acoblaments amb els tubs de guia:

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1030256366/original/Capture.PNG?1442219486)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949787/original/sa%20step%2003.PNG?1458819972)
3. Cal descargolar els dos cargols de la imatge, subjectant el motor per assegurar-vos que no caigui:

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1030256160/original/2.PNG?1442219253)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949790/original/sa%20step%2004.PNG?1458819996)


Desconnecteu el cable del motor. A continuació, es pot treure la unitat d'accionament:

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1030256590/original/Capture.PNG?1442219673)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949805/original/sa%20step%2005.PNG?1458820033)

4. Un cop fora de la unitat de transmissió antiga, podeu substituir-la per la nova. Si només heu rebut el motor, traieu el pom d'alliberament ràpid i el muntatge de la roda de pressió junt amb l'engranatge motriu de l'eix del motor antic. Munteu-los al nou motor de recanvi.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949811/original/sa%20step%2006.PNG?1458820053)

5. Finalment, torneu a instal·lar tot seguint les instruccions anteriors en ordre invers.

! Recordeu connectar els cables del motor pas a pas; en cas contrari, la unitat d'accionament no funcionarà.

* * *
### 7.8 Canvi del termistor i de la resistència

  Si voleu canviar el termistor i la resistència, també heu de treure la carcassa del capçal d’impressió. Per fer-ho, utilitzeu la clau Allen número 2,5 per treure el pern frontal encerclat a la imatge de sota de la figura 4 i, a continuació, traieu la carcassa.

![Figura 31](./IMG/img31.PNG)  
  Un cop traieu la carcassa, tindreu accés a totes les connexions del capçal d’impressió (ventilador, termistor, connexions de resistències). Les connexions dels termistors i resistències es troben a la part inferior de la carcassa del capçal d’impressió. El número 2 són les connexions de la resistència i el número 3 és per als termistors)

![Figura 32](./IMG/img32.PNG)  

  Tingueu en compte que desconnecteu el termistor i la resistència corresponents de l’extrusora que esteu substituint. Una manera fàcil de fer-ho és recordar que per a l’extrusora dreta les connexions són a la dreta i per a l’esquerra les connexions a l’esquerra.

  Un cop substituïu la resistència i el termistor que munten la carcassa, enceneu la impressora i comproveu si els extrusors s’escalfen.


### 7.9 Com reemplaçar les Unitats d'unitats de filament a la Creatr HS

* * *
![](http://creatrhs.lpfrg.com/support/solutions/articles/1000200091-how-to-change-the-drive-unit-on-creatr-hs-)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949547/original/sa%20step%2001.PNG?1458819377)

Haureu de treure les dues unitats de filament de la impressora. Fer això:

1. Descarregueu qualsevol filament que hi ha a les extrusores esquerra i dreta [Precalfeu les extrusores a 220 graus primer. Utilitzeu l'opció Descarrega filament a la pantalla de les impressores, utilitzeu un botó de retracció quan la impressora estigui connectada als controls manuals / manuals d'un programa d'impressora 3D o manteniu el passador de pressió de la unitat d'unitat mentre estireu lentament el filament amb la mà per sota el llit imprès).

Per reduir el xoc de risc, desconnecteu el cable d'alimentació de la impressora de la part posterior de la impressora abans de continuar .

2. Descargoleu els acoblaments del tub de filament de la part superior de les unitats de transmissió de filaments.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031609315/original/HS%20-%20Unscrew%20the%20Filament%20Tube%20Coupling%20-%20Solution%20Edit.PNG?1444045716)

3. Mantenint els acoblaments del tub de filament inferior cargolats a les unitats de transmissió de filaments, simplement desconnecteu els dos tubs de filament inferiors dels seus acoblaments de tubs.

4. Sostingueu la unitat d'unitat de filament per evitar que caigui i colpegi el vidre del llit d'impressió, descargoleu els 2 cargols hexagonals superiors que subjecten la unitat d'unitat de filament connectada a la part superior de la impressora (vegeu la segona imatge superior).

5. Desconnecteu el cable d'alimentació del motor de la unitat de filament a la part posterior de la unitat de filament. (A la imatge, es treu el tauler posterior per obtenir un millor accés):

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031615044/original/HS%20-%20Filament%20Drive%20Unit%20Motor%20Cable%20-%20Solution%20edit.PNG?1444049790)

Ara s’hauria de poder retirar la unitat motriu. Feu-ho també amb l’altra unitat de transmissió.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949570/original/sa%20step%2002.PNG?1458819412)

### 7.10 Com instal·lar els ventiladors de refrigeració de la unitat d’acció Filament

1. Al paquet hauríeu d'haver rebut 4 cargols hexagonals, 2 per cada ventilador. Enfileu els cargols (però no els estrenueu encara) a la part inferior de cada unitat de filament tal com es veu a la foto. Hauríeu de saber quin costat hi ha a la part inferior perquè hauríeu d’haver deixat l’acoblament del tub de filament a la part inferior. (Tot i això, aquestes fotos no mostren aquest acoblament):  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031619463/original/HS%20-%20Drive%20unit%20cooling%20fan%20screws.PNG?1444052381)

2. Col·loqueu el ventilador de refrigeració a la unitat de transmissió de filaments segons la imatge. Assegureu-vos que el cable surti per la part frontal i que les osques inferiors del suport del ventilador llisquin al voltant dels dos cargols instal·lats recentment a la part inferior:  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031623542/original/HS%20-%20drive%20unit%20cooling%20fan%20mounted%20bottom%20-%20solution%20edit.PNG?1444054623)

Els cargols que subjecten tota la unitat de transmissió a la part superior de la impressora mantindran la part superior del suport del ventilador al seu lloc. Quan el ventilador es vegi posicionat correctament, estreneu els dos cargols inferiors per evitar sorolls innecessaris. Feu-ho també amb l’altre ventilador de refrigeració.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949585/original/sa%20step%2003.PNG?1458819460)

Torneu a instal·lar les dues unitats de filament (ara amb ventiladors de refrigeració muntades) d'una en una a la impressora.  

1. Torneu a connectar el cable del motor.
2. Cargoleu els dos cargols del marc superior.
3. Connecteu el tub de filament inferior a l'acoblament del tub de filament.
4. Cargoleu l'acoblament del tub de filament superior amb el tub.
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949598/original/sa%20step%2004.PNG?1458819501)

Traieu els panells laterals i la guardapols interior. Si heu abandonat el projecte i hi torneu, SI US PLAU, TORNEU A ASSICURAR-VOS QUE EL CABLE D'ALIMENTACIÓ ESTÀ DESCONECTAT DE LA DARRERA DE LA IMPRESSORA.  

El guardapols interior:  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031625040/original/Interior%20Dust%20Cover%20-%20Solution%20Edit.PNG?1444055475)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031625181/original/Interior%20Dust%20Cover%20Side%20View-%20Solution%20Edit.PNG?1444055570)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949608/original/sa%20step%2005.PNG?1458819522)


Alimenteu els cables d’extensió dels cables als connectors adequats del ventilador. Notareu que un dels cables de connexió de l’extensió és més llarg que l’altre. Com més llarg es dirigeix ​​cap al ventilador de refrigeració esquerre; el més curt es dirigeix ​​cap al ventilador de refrigeració correcte.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031628325/original/HS%20-%20Filament%20Drive%20Unit%20Cooling%20Fan%20-%20Cable%20Extension.PNG?1444057393)

Voleu començar amb la cua a la font d'alimentació i alimentar el cable esquerre d'extensió del cable sota / darrere del fus de l'eix z posterior i darrere de tots els cables fins al marc de la cantonada inferior-darrere-esquerre . A continuació, podeu executar aquesta extensió de cable fins a la cantonada superior esquerra-esquerra i connectar-la amb el ventilador de refrigeració de la unitat de transmissió de filament esquerre. El cable de connexió correcte es pot recórrer per la cantonada inferior-darrere-dreta fins a la cantonada superior-darrere-dreta i connectar-lo al ventilador dret. Connecteu els ventiladors a l'extensió i col·loqueu la connexió de filferro i els cables a les cantonades verticals del marc.  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031631451/original/HS%20-%20Filament%20Drive%20Unit%20Cooling%20Fan%20-%20Cable%20Extension-left%20path.PNG?1444059193)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031631989/original/HS%20-%20Filament%20Drive%20Unit%20Cooling%20Fan%20-%20Cable%20Extension-left%20path2.PNG?1444059588)


(NOTA: és possible que vulgueu utilitzar una cinta elèctrica per gravar aquestes connexions juntes.)  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949620/original/sa%20step%2006.PNG?1458819548)


Connecteu l'extrem "cua" de l'extensió del cable a la font d'alimentació: el NEGRE s'ha de cargolar sota un pal NEGATIU ("-"). Cal enroscar el VERMELL sota un pol POSITIU ("+"):  

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1031632638/original/HS%20-%20Filament%20Drive%20Unit%20Cooling%20Fan%20-%20Cable%20Extension%20to%20PSU.PNG?1444059945)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/11002949627/original/sa%20step%2007.PNG?1458819565)

Torneu a col·locar la protecció contra la pols, els panells laterals i el panell posterior. Torneu a connectar el cable d'alimentació al port del cable d'alimentació de la impressora i engegueu la impressora. Els ventiladors haurien de començar a treballar!  



### 7.11 Com substituir el sensor d’inducció Z-Stop


**NOTA: No completar aquesta solució ( recalibració de la Z-Offset)  fins a la seva plenitud pot (i és probable que ho faci) danyar greument la impressora. Assegureu-vos que seguiu tots els passos proporcionats.**

1. APAGUEU LA IMPRESSORA PER A LA VOSTRA SEGURETAT PER EVITAR EL XOC. A més, traieu els panells laterals i les dues parts de la coberta electrònica de la impressora.

2. Tallar el fil gris del sensor d’inducció defectuós a prop de l’extrem del sensor.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032779874/original/HS%20-%20induction%20sensor%20-%20cut.PNG?1445515113)

3. Desenrosqueu la femella que subjecta el sensor a la muntura del sensor i traieu i rebutgeu el sensor antic.

4. Comenceu a desenllaçar el sensor del cable negre del sensor Y-Stop. Tingueu en compte els forats del muntatge del cargol que ha de recórrer el cable del sensor d’inducció. És possible que hagueu de tallar lligams (en algunes impressores) que uneixen el cable del sensor d’inducció als feixos que recorren la part inferior del marc i la part frontal de la placa base.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032780448/original/HS%20-%20induction%20sensor%20-%20vertical%20frame%20binding.PNG?1445515547)
![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032781872/original/HS%20-%20induction%20sensor%20-%20wire%20route%20left%20side1.PNG?1445516796)


![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032781389/original/HS%20-%20induction%20sensor%20-%20wire%20route%20left%20side.PNG?1445516361)

Els 3 cables es divideixen a la placa base: el negre va a la placa base, el blau i el marró s’envien a la font d’alimentació. Continueu deslligant el

5. Desconnecteu la connexió de fil negre de la placa base; assegureu-vos que sigui el fil negre i prim que es mostra a la foto següent:

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032783532/original/HS%20-%20induction%20sensor%20-%20motherboard%20connection2.PNG?1445518099)


6. Notareu que el fil blau està unit a un altre fil negatiu negre i el fil marró a un altre fil positiu vermell. Retalleu els vells cables blaus i marrons deixant el vermell i el negre als connectors pin-splice. Ara el cable gris hauria de ser lliure. També podeu descartar-ho.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032779437/original/HS%20-%20Z%20induction%20sensor%20PSU%20hook%20up.PNG1445514714)

7. Haureu de ser capaç d'instal·lar el sensor d'inducció de recanvi. Comenceu per la part superior, assegurant l'extrem del senor al muntatge del senor amb la rentadora i la femella. És possible que hagueu de baixar el llit imprès; podeu fer-ho amb la mà girant lentament la corretja de l'eix Z en un sentit antihorari (això no danyarà la impressora). Voldreu apretar el sensor de manera que el LED del sensor estigui orientat cap a la part frontal o cap a la dreta (de manera que sigui visible). És possible que també hàgiu d’ajustar la femella superior perquè surti més part de la punta del sensor per la femella inferior. No us preocupeu de calibrar-lo durant aquest pas; es farà al final.

8. Enllaceu el cable nou amb el cable del sensor Y-Stop per la cantonada vertical del marc fins arribar al muntatge de l'eix inferior. Enfileu el nou fil gris (i els seus 3 fils més petits) pel petit forat de la cantonada. És possible que hàgiu de treure (amb cura) el cable negre del connector per aconseguir que encaixi a través de tot aquest conjunt  heu de tenir en compte el forat del connector que hi ha el cable negre petit del sensor); el podeu deixar apagat fins que arribi el moment de connectar el cable negre a la placa base.

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032780875/original/HS%20-%20induction%20sensor%20-%20spindle%20hole.PNG?1445515879)

9. Continueu lligant el fil gris amb el paquet més gran fins que arribi a la cantonada frontal de la placa base. Executeu-lo pels 6 connectors del motor pas a pas (negre, verd, vermell, blau) i enllaceu-lo als cables (algunes impressores només tenen unió de cables per unir aquest grup de cables; és possible que vulgueu adquirir-ne alguns localment per enllaçar-los de nou). .

10. A la cantonada frontal dreta de la placa base, el fil negre es separa del fil blau i marró. Torneu a connectar el cable negre al connector que venia amb la peça de recanvi (i que es va desconnectar al pas 08). Vegeu la imatge següent per saber a quina ranura entra el cable al connector. Connecteu també el connector a la placa base a la ranura correcta (també es veu a la imatge següent):

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032776394/original/HS%20-%20Z%20induction%20sensor%20Motherboard%20connection.PNG?1445511645)

Un cable baix! Dos per acabar!

11. Ara busqueu l'enquadernació que va a la unitat d'alimentació. Uniu el blau i el negre fins que arribi a la font d'alimentació (és possible que tingueu una mica d'excés).

12. EL BLA VA A NEGATIU ("-"): amb l'apagat, utilitzeu un tornavís de cap Phillips (creuat) per afluixar un terminal negatiu. Introduïu el cable blau, estrenyeu el terminal i estireu-lo suaument per confirmar que està segur. Torneu a ajustar-lo si cal. (Vegeu la imatge del pas 06.)

13. EL MARRÓ VA A POSITIU ("+"): afluixeu un terminal positiu, introduïu el cable marró, estrenyiu el cargol del terminal i estireu suaument aquest cable per confirmar que està segur. Torneu a ajustar-lo si cal. (Vegeu la imatge del pas 06.)

14. Enceneu la impressora. Proveu el sensor amb la punta del tornavís per tocar l'extrem del sensor d'inducció. Hi ha dos LED implicats en aquest sensor: un al propi sensor i un a la placa base. Quan el del sensor estigui al de la placa base s’ha d’apagar. NO, NO, ABSOLUTAMENT NO HOME LA IMPRESSORA .

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032778350/original/HS%20-%20Z%20induction%20sensor%20test.PNG?1445513591)

![](https://s3.amazonaws.com/cdn.freshdesk.com/data/helpdesk/attachments/production/1032778489/original/hs%20-%20z%20inductions%20sensor%20Motherboard%20LED.PNG?1445513762)


(L'enquadernació a la foto esquerra és incorrecta; l'enquadernació en negre hauria de ser al sensor d'inducció Z-Stop, no al sensor Y-Stop) i ha d'estar lligada gairebé fins a la part metàl·lica del sensor d'inducció. útil només per a la prova esmentada al pas 14).

16. PAS MOLT IMPORTANT \ - Ara haureu de tornar a calibrar el Z-Offset ara. NO HOMEU LA IMPRESSORA ABANS DE RE-CALIBRAR LA Z-OFFSET. En cas contrari, el vidre del llit d’impressió pot prémer els nozzles del capçal d’impressió, el vidre, els nozzles, el capçal d’impressió i / o els motors de l’eix z. Aquí teniu l’enllaç per calibrar la Z-Offset a les impressores Creatr HS: [Com calibrar la Z-Offset de la Creatr HS]
* * *

### 7.12 Substituir Rotary encoder
<img src="./IMG/encoder1.jpg" width="200"/>
<img src="./IMG/encoder2.jpg" width="200"/>


# 3. Millores y curiositats

**Millores**
Coixinets
  - Millora precisió eix X i Y
  - Reducció soroll
  - Proposta

Roda dentada extrusor
  - Major control sobre moviment filament
  - Proposta

Canvi de placa base
  - Millora precisió y control de la impressora
  - Proposta


Canvi de Rotary encoder
    -Major precisió per moure-us per la pantalla


**Curiositat**

Aquesta impressora, que una vegada que comença la impressió podem retirar l'USB que troba connectat, ja que guarda el gcode en la placa OLIMEX que té integrada.
